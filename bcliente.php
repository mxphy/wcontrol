<?php
$webid = 'clientes';
include_once "app/iniciar.php";
?>

<!doctype html>
<html lang="en">

<head>
	<title>Buscar socio | wControl</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- CSS -->
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/vendor/icon-sets.css">
	<link rel="stylesheet" href="assets/css/<?php echo $color?>">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="assets/css/demo.css">
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
</head>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- SIDEBAR -->
		<div class="sidebar">
			<div class="brand">
				<a href="index.php">
					<center>
						<font color="white">
					<h4><img src="assets/img/logoini.png"<br> <?php echo $nombreclub?></h4>
				</font>
				</center>
			</a>
			</div>
			<div class="sidebar-scroll">
				<nav>
					<?php include "assets/menu.php"; ?>
				</nav>
			</div>

		</div>
		<!-- END SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
			<!-- NAVBAR -->
			<nav class="navbar navbar-default">
				<div class="container-fluid">
					<div class="navbar-btn">
						<button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-arrow-left-circle"></i></button>
					</div>
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-menu">
							<span class="sr-only">Toggle Navigation</span>
							<i class="fa fa-bars icon-nav"></i>
						</button>
					</div>
					<div id="navbar-menu" class="navbar-collapse collapse">
						<ul class="nav navbar-nav navbar-right">
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="assets/img/user.png" class="img-circle" alt="Avatar"> <span><?php echo $nombreclub?></span> <i class="icon-submenu lnr lnr-chevron-down"></i></a>
								<ul class="dropdown-menu">
									<li><a href="assets/logout.php"><i class="lnr lnr-exit"></i> <span>Salir</span></a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</nav>
			<!-- END NAVBAR -->
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<center>
					<h3 class="page-title">Buscar socio</h3>
					<div class="panel panel-headline">
						<div class="panel-body">
							<?php
      				 $buscar=$_POST['busqueda'];

      					if (!isset($buscar)){
      							echo "Debe especificar un nombre para realizar la búsqueda <br> <a href='buscarcliente.php'>Click aquí para volver</a>";
      							echo "</html></body> \n";
      							exit;
      					}
      					$sql = "SELECT * FROM socios WHERE nombre LIKE '%$buscar%' ORDER BY id DESC";
      					$result = mysqli_query($lin, $sql);
								if ($result === false) {
										echo "<option>Could not successfully run query ($sql) from DB: " . mysql_error();
										echo "</option>";
										exit;
								}
								if (mysqli_num_rows($result) == 0) {
	      					echo "¡No se ha encontrado ningún cliente registrado con este nombre! <br> Recuerda que las tildes influyen en la búsqueda. <br><br>  <a href='buscarcliente.php'>Click aquí para volver</a>";
	      					exit();
								}
      					echo "<table class='table'> \n";
      					//Mostramos los nombres de las tablas
      					while ($row = mysqli_fetch_assoc($result)){

                          echo "<tr><td><strong>Nº de socio:</strong> ".$row["socio"]."</td></tr> \n";
                          echo "<tr><td><strong>Nombre:</strong> ".$row["nombre"]."</td></tr> \n";
      										echo "<tr><td><strong>Email:</strong> ".$row["email"]."</td></tr> \n";
      										echo "<tr><td><strong>Teléfono:</strong> ".$row["telefono"]."</td></tr> \n";
      										echo "<tr><td><strong>DNI:</strong> ".$row["dni"]."</td></tr> \n";
      										echo "<tr><td><strong>Fecha de nacimiento:</strong> ".$row["nacimiento"]."</td></tr> \n";
      										echo "<tr><td><strong>Fecha de inscripción:</strong> ".$row["inscripcion"]."</td></tr> \n";
      										echo "<tr><td><strong>Precio pagado:</strong> ".$row["precio"]."</td></tr> \n";
      										echo "<tr><td><strong>Avalador:</strong> ".$row["avalador"]."</td></tr> \n";
                          echo "<tr><td><strong>Dirección:</strong> ".$row["direccion"]."</td></tr> \n";
                          echo "<tr><td><strong>Notas:</strong> ".$row["notas"]."</td></tr> \n";
                          echo "<tr><td>".$row["separacion"]."</td></tr> \n";

      							}
      										echo "</table> \n";?>

						</div>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
			<footer>
				<div class="container-fluid">
					<p class="copyright">&copy; wControl 2017 - Developed by BlackLeaf (<?php echo $version?>)</p>
				</div>
			</footer>
		</div>
		<!-- END MAIN -->
	</div>
	<!-- END WRAPPER -->
	<!-- Javascript -->
	<script src="assets/js/jquery/jquery-2.1.0.min.js"></script>
	<script src="assets/js/bootstrap/bootstrap.min.js"></script>
	<script src="assets/js/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="assets/js/klorofil.min.js"></script>
</body>

</html>
