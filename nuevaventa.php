<?php
$webid = 'ventas';
include_once "app/iniciar.php";
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	$preciofinal = '0';
	for ($i=0; $i < 6; $i++) {
		if (!empty($_POST['producto'][$i]['cliente'])) {
			$precio0 = WControl::ObtenerPrecio($_POST['producto'][$i]['cliente']) * $_POST['producto'][$i]['cantidad'];
			$preciofinal = $preciofinal + $precio0;
		}
		else {
				$_POST['producto'][$i]['cliente'] = "-1";
		}
		if (!isset($_POST['producto'][$i]['cliente'])) {
			$_POST['producto'][$i]['cliente'] = "-1";
		}
	}
	for ($i=0; $i < 6; $i++) {
		if (!empty($_POST['producto'][$i]['cantidad'])) {
			WControl::ActualizarStock($_POST['producto'][$i]['cliente'], $_POST['producto'][$i]['cantidad']);
		}
		else {
				$_POST['producto'][$i]['cantidad'] = "0";
		}
		if (!isset($_POST['producto'][$i]['cantidad'])) {
			$_POST['producto'][$i]['cantidad'] = "0";
		}
	}
	$fecha = WControl::ObtenerFecha();
	$sql = "INSERT INTO `ventas` (`socio`, `producto0`, `cantidad0`, `producto1`, `cantidad1`, `producto2`, `cantidad2`, `producto3`, `cantidad3`, `producto4`, `cantidad4`, `producto5`, `cantidad5`, `preciofinal`, `fecha`, `vendedor`)
	VALUES ('".$_POST['numsocio']."',
		'".$_POST['producto'][0]['cliente']."',
		'".$_POST['producto'][0]['cantidad']."',
		'".$_POST['producto'][1]['cliente']."',
		'".$_POST['producto'][1]['cantidad']."',
		'".$_POST['producto'][2]['cliente']."',
		'".$_POST['producto'][2]['cantidad']."',
		'".$_POST['producto'][3]['cliente']."',
		'".$_POST['producto'][3]['cantidad']."',
		'".$_POST['producto'][4]['cliente']."',
		'".$_POST['producto'][4]['cantidad']."',
		'".$_POST['producto'][5]['cliente']."',
		'".$_POST['producto'][5]['cantidad']."',
		'".$preciofinal."',
		'".$fecha."',
		'".$_POST['vendedor']."');";
	 $query = mysql_query($sql);
	 if ($query === false) {
		 echo "Could not successfully run query ($sql) from DB: " . mysql_error();
		 exit;
	 }
	 else {
			$exito = "¡Venta creada satisfactoriamente, TOTAL A COBRAR: € ".$preciofinal."!";
			WControl::EscribirLog("Se ha realizado una venta de €".$preciofinal." al socio ".$_POST['numsocio'].". Venta atendida por ".$_POST['vendedor'].".");
	 }
}


?>

<!doctype html>
<html lang="en">

<head>
	<title>Ventas | wControl</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- CSS -->
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/vendor/icon-sets.css">
	<link rel="stylesheet" href="assets/css/<?php echo $color?>">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="assets/css/demo.css">
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
</head>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- SIDEBAR -->
		<div class="sidebar">
			<div class="brand">
				<a href="index.php">
				  <center>
				    <font color="white">
				  <h4><img src="assets/img/logoini.png"<br> <?php echo $nombreclub?></h4>
				</font>
				</center>
				</a>
			</div>
			<div class="sidebar-scroll">
				<nav>
					<?php include "assets/menu.php"; ?>
				</nav>
			</div>

		</div>
		<!-- END SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
			<!-- NAVBAR -->
			<nav class="navbar navbar-default">
				<div class="container-fluid">
					<div class="navbar-btn">
						<button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-arrow-left-circle"></i></button>
					</div>
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-menu">
							<span class="sr-only">Toggle Navigation</span>
							<i class="fa fa-bars icon-nav"></i>
						</button>
					</div>
					<div id="navbar-menu" class="navbar-collapse collapse">

						<ul class="nav navbar-nav navbar-right">


							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="assets/img/user.png" class="img-circle" alt="Avatar"> <span><?php echo $nombreclub?></span> <i class="icon-submenu lnr lnr-chevron-down"></i></a>
								<ul class="dropdown-menu">
									<li><a href="assets/logout.php"><i class="lnr lnr-exit"></i> <span>Salir</span></a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</nav>
			<!-- END NAVBAR -->
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<center>
						<?php if (isset($exito)) {
              printf('<div class="alert alert-info" role="alert">%s</div>', $exito);
            }?>
					<h3 class="page-title">Nueva venta<br>
          <small><small><font color="red">Todos los campos con * son obligatorios</font></small></small></h3>
					<div class="panel panel-headline">
						<div class="panel-body">
              <form action="<?= $_SERVER['PHP_SELF'] ?>" method="post">

                <input type="text" class="form-control" name="socio" style="text-align: center;" value="#0001" readonly="true"><br>
                <label for="cliente">Numero de Socio *</label>
                <select class="form-control" style="text-align: center;" name="numsocio">
                  <?php $sql = "SELECT * FROM socios";
                    $query = mysql_query($sql);
                    if ($query === false) {
                        echo "<option>Could not successfully run query ($sql) from DB: " . mysql_error();
                        echo "</option>";
                        exit;
                    }
                    while ($row = mysql_fetch_assoc($query)) {
                      $numsocio = $row['socio'];
                      printf('<option value="%s">%s</option>', $numsocio, $numsocio);
                    }?>

                </select><br>
                <label for="cliente">Productos (Máximo 10) *</label>
                <div class="form-group mt-repeater">
                    <div data-repeater-list="producto">
                        <div data-repeater-item class="mt-repeater-item">
                            <div class="row mt-repeater-row">
                                <div class="col-md-8">
                                    <label class="control-label">Producto</label>
                                    <select class="form-control" style="text-align: center;" name="cliente">
                                      <?php $sql = "SELECT * FROM inventario";
                                        $query = mysql_query($sql);
                                        if ($query === false) {
                                            echo "<option>Could not successfully run query ($sql) from DB: " . mysql_error();
                                            echo "</option>";
                                            exit;
                                        }
                                        while ($row = mysql_fetch_assoc($query)) {
                                          $numproducto = $row['id'];
                                          $nombreproducto = $row['nombre'];
                                          printf('<option value="%s">%s - %s</option>', $numproducto, $numproducto, $nombreproducto);
                                        }?>

                                    </select> </div>
                                <div class="col-md-3">
                                    <label class="control-label">Cantidad en unidades/gramos</label>
                                    <input type="text" placeholder="0.3" class="form-control" name="cantidad" /> </div>
                                <div class="col-md-1">
																	<label class="control-label">Accion</label>
                                    <a href="javascript:;" data-repeater-delete class="btn btn-danger mt-repeater-delete">
                                        <i class="fa fa-close"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <a href="javascript:;" data-repeater-create class="btn btn-info mt-repeater-add">
                        <i class="fa fa-plus"></i> Añadir producto</a>
                </div>
                <input type="text" class="form-control" name="socio" style="text-align: center;" placeholder="Detalles"><br>
								<input type="text" class="form-control" name="vendedor" style="text-align: center;" placeholder="Vendedor" required><br>


                <input type="submit" class="btn btn-primary" name="enviar" value="Finalizar">
              </form>

						</div>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN -->
	</div>
	<!-- END WRAPPER -->
	<!-- Javascript -->
	<script src="assets/js/jquery/jquery-2.1.0.min.js"></script>
	<script src="assets/js/bootstrap/bootstrap.min.js"></script>
	<script src="assets/js/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="assets/js/klorofil.min.js"></script>
  <script src="assets/js/repetidor.js" type="text/javascript"></script>
  <script src="assets/js/confrepetidor.js" type="text/javascript"></script>

</body>

</html>
