<?php
$webid = 'seguridad';
include_once "app/iniciar.php";

$fecha = WControl::ObtenerFecha();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	if(isset($_POST['nombre'])){
		$nombrenuevo = $_POST['nombreclub'];
		$sql = "UPDATE `admin` SET `nombre`='$nombrenuevo' WHERE user = '$username'";
		$query = mysql_query($sql);
		if ($query === false) {
	      echo "Could not successfully run query ($sql) from DB: " . mysql_error();
	      exit;
	  }
	  else {
	    $msg = "¡Exito! El nombre del sitio ha sido modificado correctamente. (Empezará a funcionar a partir de que cambies de sección)";
	    WControl::EscribirLog("Se ha cambiado el nombre del sitio a ".$nombrenuevo.";");
	  }

	}

	if(isset($_POST['color'])){
		$colornuevo = $_POST['colornuevo'];
		$sql = "UPDATE `admin` SET `color`='$colornuevo' WHERE user = '$username'";
		$query = mysql_query($sql);
		if ($query === false) {
	      echo "Could not successfully run query ($sql) from DB: " . mysql_error();
	      exit;
	  }
	  else {
	    $msg = "¡Exito! El color del sitio se ha modificado correctamente. (Empezará a funcionar a partir de que cambies de sección)<br>
			<small>Si no funciona, borra archivos e imágenes almacenadas en caché desde el historial de tu ordenador</small>";
	    WControl::EscribirLog("Se ha modificado el color del sitio a ".$colornuevo.";");
	  }

	}

	}

?>

<!doctype html>
<html lang="en">

<head>
	<title>Changelog | wControl</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- CSS -->
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/vendor/icon-sets.css">
	<link rel="stylesheet" href="assets/css/<?php echo $color?>">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="assets/css/demo.css">
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
</head>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- SIDEBAR -->
		<div class="sidebar">
			<div class="brand">
				<a href="index.php">
				  <center>
				    <font color="white">
				  <h4><img src="assets/img/logoini.png"<br> <?php echo $nombreclub?></h4>
				</font>
				</center>
				</a>
			</div>
			<div class="sidebar-scroll">
				<nav>
					<?php include "assets/menu.php"; ?>
				</nav>
			</div>

		</div>
		<!-- END SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
			<!-- NAVBAR -->
			<nav class="navbar navbar-default">
				<div class="container-fluid">
					<div class="navbar-btn">
						<button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-arrow-left-circle"></i></button>
					</div>
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-menu">
							<span class="sr-only">Toggle Navigation</span>
							<i class="fa fa-bars icon-nav"></i>
						</button>
					</div>
					<div id="navbar-menu" class="navbar-collapse collapse">

						<ul class="nav navbar-nav navbar-right">


							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="assets/img/user.png" class="img-circle" alt="Avatar"> <span><?php echo $nombreclub?></span> <i class="icon-submenu lnr lnr-chevron-down"></i></a>
								<ul class="dropdown-menu">
									<li><a href="assets/logout.php"><i class="lnr lnr-exit"></i> <span>Salir</span></a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</nav>
			<!-- END NAVBAR -->
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<?php if (isset($msg)) {
          printf('<div class="alert alert-info" role="alert">
  <i class="fa fa-exclamation-circle" aria-hidden="true"></i>
  %s
</div>', $msg);}?>
				<div class="container-fluid">
						<div class="panel panel-default">
						  <div class="panel-heading" style="text-align: center;">Configuración del sitio</div>
						  <div class="panel-body">
								<center>
									<form action="<?= $_SERVER['PHP_SELF'] ?>" method="post">
									<b>Cambiar el nombre de el club cannábico</b><br>
									<input type="text" class="form-control" name="nombreclub" size="400" placeholder="Ejemplo: wControl" style="text-align: center;" required>
									<br><input type="submit" class="btn btn-primary" value="Cambiar" name="nombre"><hr>
								</form>
								<form action="<?= $_SERVER['PHP_SELF'] ?>" method="post">
								<b>Cambiar color del diseño</b><br>
								<small>Si no funciona, borra archivos e imágenes almacenadas en caché desde el historial de tu ordenador</small>
								<div class="form-group">
							  <select class="form-control" id="colornuevo" name="colornuevo">
							    <option value="azul">Azul</option>
							    <option value="verde">Verde</option>
							    <option value="rojo">Rojo</option>
							    <option value="violeta">Violeta</option>
							  </select>
							</div>
								<input type="submit" class="btn btn-primary" value="Cambiar" name="color"><hr>
							</form>
						</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
			<footer>
				<div class="container-fluid">

				</div>
			</footer>
		</div>
		<!-- END MAIN -->
	</div>
	<!-- END WRAPPER -->
	<!-- Javascript -->
	<script src="assets/js/jquery/jquery-2.1.0.min.js"></script>
	<script src="assets/js/bootstrap/bootstrap.min.js"></script>
	<script src="assets/js/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="assets/js/klorofil.min.js"></script>
</body>

</html>
