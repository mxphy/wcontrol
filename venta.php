<?php
$webid = 'ventas';
include_once "app/iniciar.php";

if (isset($_GET['id'])) {
		$buscar = $_GET['id'];
}
else {
		header('Location: historial.php');
		exit();
}

?>

<!doctype html>
<html lang="en">

<head>
	<title>Inicio | wControl</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- CSS -->
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/vendor/icon-sets.css">
	<link rel="stylesheet" href="assets/css/<?php echo $color?>">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="assets/css/demo.css">
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
</head>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- SIDEBAR -->
		<div class="sidebar">
			<div class="brand">
				<a href="index.php">
				  <center>
				    <font color="white">
				  <h4><img src="assets/img/logoini.png"<br> <?php echo $nombreclub?></h4>
				</font>
				</center>
				</a>
			</div>
			<div class="sidebar-scroll">
				<nav>
					<?php include "assets/menu.php"; ?>
				</nav>
			</div>

		</div>
		<!-- END SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
			<!-- NAVBAR -->
			<nav class="navbar navbar-default">
				<div class="container-fluid">
					<div class="navbar-btn">
						<button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-arrow-left-circle"></i></button>
					</div>
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-menu">
							<span class="sr-only">Toggle Navigation</span>
							<i class="fa fa-bars icon-nav"></i>
						</button>
					</div>
					<div id="navbar-menu" class="navbar-collapse collapse">

						<ul class="nav navbar-nav navbar-right">


							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="assets/img/user.png" class="img-circle" alt="Avatar"> <span><?php echo $nombreclub?></span> <i class="icon-submenu lnr lnr-chevron-down"></i></a>
								<ul class="dropdown-menu">
									<li><a href="assets/logout.php"><i class="lnr lnr-exit"></i> <span>Salir</span></a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</nav>
			<!-- END NAVBAR -->
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<center>
					<h3 class="page-title">Registro de venta</h3>
					<div class="panel panel-headline">
						<div class="panel-body">
              <?php
							$sql = "SELECT * FROM ventas WHERE id = '$buscar'";
      					$query = mysql_query($sql);
								while ($row = mysql_fetch_array($query)) {
									$ventaid = $row['id'];
									$socio = $row['socio'];
									$preciofin = $row['preciofinal'];
									$fechav = $row['fecha'];
									$producto0 = array('prod' => $row['producto0'], 'cant' => $row['cantidad0']);
									$producto1 = array('prod' => $row['producto1'], 'cant' => $row['cantidad1']);
									$producto2 = array('prod' => $row['producto2'], 'cant' => $row['cantidad2']);
									$producto3 = array('prod' => $row['producto3'], 'cant' => $row['cantidad3']);
									$producto4 = array('prod' => $row['producto4'], 'cant' => $row['cantidad4']);
									$producto5 = array('prod' => $row['producto5'], 'cant' => $row['cantidad5']);
								}
								printf('<h2>Venta #%s (%s)</h2>', $ventaid, $fechav);
								printf('<h5>Cliente: %s</h5>', $socio);
      					?>
								<table class="table">
									<thead>
										<tr>
											<td>Producto</td>
											<td>Cantidad</td>
											<td>Precio por unidad/gramo</td>
											<td>Total</td>
										</tr>
									</thead>
									<tbody>
										<?php if ($producto0['prod'] != -1) {
											$sql = "SELECT * FROM inventario WHERE id = '".$producto0['prod']."'";
											$query = mysql_query($sql);
											while ($row = mysql_fetch_assoc($query)) {
												$preciog = $row['precio'];
												$nombrepr = $row['nombre'];
											}
											$prodprecio0 = $producto0['cant'] * $preciog;
											printf('<tr>
												<td>%s (#%s)</td>
												<td>%sg</td>
												<td>€ %s</td>
												<td>€ %s</td>
											</tr>', $nombrepr, $producto0['prod'], $producto0['cant'], $preciog, $prodprecio0);
										} ?>
										<?php if ($producto1['prod'] != -1) {
											$sql = "SELECT * FROM inventario WHERE id = '".$producto1['prod']."'";
											$query = mysql_query($sql);
											while ($row = mysql_fetch_assoc($query)) {
												$preciog = $row['precio'];
												$nombrepr = $row['nombre'];
											}
											$prodprecio1 = $producto1['cant'] * $preciog;
											printf('<tr>
												<td>%s (#%s)</td>
												<td>%s</td>
												<td>€ %s</td>
												<td>€ %s</td>
											</tr>', $nombrepr, $producto1['prod'], $producto1['cant'], $preciog, $prodprecio1);
										} ?>
										<?php if ($producto2['prod'] != -1) {
											$sql = "SELECT * FROM inventario WHERE id = '".$producto2['prod']."'";
											$query = mysql_query($sql);
											while ($row = mysql_fetch_assoc($query)) {
												$preciog = $row['precio'];
												$nombrepr = $row['nombre'];
											}
											$prodprecio2 = $producto2['cant'] * $preciog;
											printf('<tr>
												<td>%s (#%s)</td>
												<td>%s</td>
												<td>€ %s</td>
												<td>€ %s</td>
											</tr>', $nombrepr, $producto2['prod'], $producto2['cant'], $preciog, $prodprecio2);
										} ?>
										<?php if ($producto3['prod'] != -1) {
											$sql = "SELECT * FROM inventario WHERE id = '".$producto3['prod']."'";
											$query = mysql_query($sql);
											while ($row = mysql_fetch_assoc($query)) {
												$preciog = $row['precio'];
												$nombrepr = $row['nombre'];
											}
											$prodprecio3 = $producto3['cant'] * $preciog;
											printf('<tr>
												<td>%s (#%s)</td>
												<td>%s</td>
												<td>€ %s</td>
												<td>€ %s</td>
											</tr>', $nombrepr, $producto3['prod'], $producto3['cant'], $preciog, $prodprecio3);
										} ?>
										<?php if ($producto4['prod'] != -1) {
											$sql = "SELECT * FROM inventario WHERE id = '".$producto4['prod']."'";
											$query = mysql_query($sql);
											while ($row = mysql_fetch_assoc($query)) {
												$preciog = $row['precio'];
												$nombrepr = $row['nombre'];
											}
											$prodprecio4 = $producto4['cant'] * $preciog;
											printf('<tr>
												<td>%s (#%s)</td>
												<td>%s</td>
												<td>€ %s</td>
												<td>€ %s</td>
											</tr>', $nombrepr, $producto4['prod'], $producto4['cant'], $preciog, $prodprecio4);
										} ?>
										<?php if ($producto5['prod'] != -1) {
											$sql = "SELECT * FROM inventario WHERE id = '".$producto5['prod']."'";
											$query = mysql_query($sql);
											while ($row = mysql_fetch_assoc($query)) {
												$preciog = $row['precio'];
												$nombrepr = $row['nombre'];
											}
											$prodprecio5 = $producto5['cant'] * $preciog;
											printf('<tr>
												<td>%s (#%s)</td>
												<td>%s</td>
												<td>€ %s</td>
												<td>€ %s</td>
											</tr>', $nombrepr, $producto5['prod'], $producto5['cant'], $preciog, $prodprecio5);
										} ?>
									</tbody>
								</table>
								<?php printf('<h3>Precio final : € %s</h3>', $preciofin); ?>
						</div>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
			<footer>
				<div class="container-fluid">
					<p class="copyright">&copy; wControl 2017 - Developed by BlackLeaf (<?php echo $version?>)</p>

				</div>
			</footer>
		</div>
		<!-- END MAIN -->
	</div>
	<!-- END WRAPPER -->
	<!-- Javascript -->
	<script src="assets/js/jquery/jquery-2.1.0.min.js"></script>
	<script src="assets/js/bootstrap/bootstrap.min.js"></script>
	<script src="assets/js/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="assets/js/klorofil.min.js"></script>
</body>

</html>
