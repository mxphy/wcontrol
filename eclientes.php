<?php
$webid = 'clientes';
include_once "app/iniciar.php";
?>

<!doctype html>
<html lang="en">

<head>
	<title>Editar cliente | wControl</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- CSS -->
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/vendor/icon-sets.css">
	<link rel="stylesheet" href="assets/css/<?php echo $color?>">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="assets/css/demo.css">
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
</head>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- SIDEBAR -->
		<div class="sidebar">
			<div class="brand">
				<a href="index.php">
				  <center>
				    <font color="white">
				  <h4><img src="assets/img/logoini.png"<br> <?php echo $nombreclub?></h4>
				</font>
				</center>
				</a>
			</div>
			<div class="sidebar-scroll">
				<nav>
					<?php include "assets/menu.php"; ?>
				</nav>
			</div>

		</div>
		<!-- END SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
			<!-- NAVBAR -->
			<nav class="navbar navbar-default">
				<div class="container-fluid">
					<div class="navbar-btn">
						<button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-arrow-left-circle"></i></button>
					</div>
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-menu">
							<span class="sr-only">Toggle Navigation</span>
							<i class="fa fa-bars icon-nav"></i>
						</button>
					</div>
					<div id="navbar-menu" class="navbar-collapse collapse">

						<ul class="nav navbar-nav navbar-right">


							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="assets/img/user.png" class="img-circle" alt="Avatar"> <span><?php echo $nombreclub?></span> <i class="icon-submenu lnr lnr-chevron-down"></i></a>
								<ul class="dropdown-menu">
									<li><a href="assets/logout.php"><i class="lnr lnr-exit"></i> <span>Salir</span></a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</nav>
			<!-- END NAVBAR -->
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<center>
					<h3 class="page-title">Buscar socio</h3>
					<div class="panel panel-headline">
						<div class="panel-body">
              <?php
    								include('assets/scripts/conexpca.php');//incluye el archivo php que contiene la conexion
    								$con=Conectar();//variable que almacena la conexión ala base de datos
    								if(isset($_REQUEST['buscar'])){
    								$nombre=$_REQUEST['nombre'];
    								$query="select * from socios where socio='$nombre'";
    								$cierto=mysql_query($query,$con);//ejecutando consulta


    								if(!$cierto){
    								echo "<br>El socio introducido no existe.<br> <a href='editarcliente.php'>Regresar</a>";
    								}else
    								{
    								if($row=mysql_fetch_array($cierto)){
    								echo "<form action='eclientefinals.php' method='post'>

										<input class='form-control' name='socio' type='hidden' value='$row[socio]'><br>
                    Nombre: <input class='form-control' name='nombre' type='text' value='$row[nombre]'><br>
    								Email: <input class='form-control' name='email' type='text' value='$row[email]'><br>
    								Teléfono: <input class='form-control' name='telefono' type='text' value='$row[telefono]'><br>
    								DNI: <input class='form-control' name='dni' type='text' value='$row[dni]'><br>
    								Fecha de nacimiento: <input class='form-control' name='nacimiento' type='text' value='$row[nacimiento]'><br>
    								Fecha de inscripción: <input class='form-control' name='inscripcion' type='text' value='$row[inscripcion]'><br>
    								Precio pagado: <input class='form-control' name='precio' type='text' value='$row[precio]'><br>
                    Avalador: <input class='form-control' name='avalador' type='text' value='$row[avalador]'><br>
                    Dirección: <input class='form-control' name='direccion' type='text' value='$row[direccion]'><br>
                    Notas: <input class='form-control' name='notas' type='text' value='$row[notas]'><br>


    								<input type='submit' name='Modificar' value='Modificar' class='btn btn-info'>
    								</form>";
    								}
    								else{
    								echo "<br>El socio introducido no existe.<br> <a href='editarcliente.php'>Regresar</a>";
    								}
    								}
    								}
    								?>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
			<footer>
				<div class="container-fluid">
					<p class="copyright">&copy; wControl 2017 - Developed by BlackLeaf (<?php echo $version?>)</p>

				</div>
			</footer>
		</div>
		<!-- END MAIN -->
	</div>
	<!-- END WRAPPER -->
	<!-- Javascript -->
	<script src="assets/js/jquery/jquery-2.1.0.min.js"></script>
	<script src="assets/js/bootstrap/bootstrap.min.js"></script>
	<script src="assets/js/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="assets/js/klorofil.min.js"></script>
</body>

</html>
