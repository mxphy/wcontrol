<?php
ob_start();
$webid = 'inventario';
include_once "app/iniciar.php";
if (!isset($_GET['editid'])) {
  header('Location: inventario.php?error=Ineditable');
  exit();
}
if (!is_numeric($_GET['editid'])) {
  header('Location: inventario.php?error=Ineditable');
  exit();
}


?>

<!doctype html>
<html lang="en">

<head>
	<title>Edicion | wControl</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- CSS -->
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/vendor/icon-sets.css">
  <link rel="stylesheet" href="assets/css/<?php echo $color?>">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="assets/css/demo.css">
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
</head>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- SIDEBAR -->
		<div class="sidebar">
			<div class="brand">
        <a href="index.php">
          <center>
            <font color="white">
          <h4><img src="assets/img/logoini.png"<br> <?php echo $nombreclub?></h4>
        </font>
        </center>
        </a>
			</div>
			<div class="sidebar-scroll">
				<nav>
					<?php include "assets/menu.php"; ?>
				</nav>
			</div>

		</div>
		<!-- END SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
			<!-- NAVBAR -->
			<nav class="navbar navbar-default">
				<div class="container-fluid">
					<div class="navbar-btn">
						<button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-arrow-left-circle"></i></button>
					</div>
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-menu">
							<span class="sr-only">Toggle Navigation</span>
							<i class="fa fa-bars icon-nav"></i>
						</button>
					</div>
					<div id="navbar-menu" class="navbar-collapse collapse">

						<ul class="nav navbar-nav navbar-right">


							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="assets/img/user.png" class="img-circle" alt="Avatar"> <span><?php echo $nombreclub?></span> <i class="icon-submenu lnr lnr-chevron-down"></i></a>
								<ul class="dropdown-menu">
									<li><a href="assets/logout.php"><i class="lnr lnr-exit"></i> <span>Salir</span></a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</nav>
			<!-- END NAVBAR -->
			<!-- MAIN CONTENT -->
			<div class="main-content">
        <?php if (isset($msg)) {
          printf('<div class="alert alert-info" role="alert">
  <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
  %s
</div>', $msg);
        } ?>
				<div class="container-fluid">
					<center>

					<h3 class="page-title">Editar un producto<br>
          <small><small><font color="red">Todos los campos con * son obligatorios</font></small></small></h3>
					<div class="panel panel-headline">
						<div class="panel-body">
              <form action="inventario.php?edit=true" method="post">
                <?php $sql = "SELECT * FROM inventario WHERE id = '" . $_GET['editid'] . "'";
                $query = mysql_query($sql);
                if ($query === false || mysql_num_rows($query) <= 0) {
                  header('Location: inventario.php?error=Ineditable');
                  exit();
                }
                while ($row = mysql_fetch_assoc($query)) {
                    $res1 = $row['nombre'];
                    $res2 = $row['precio'];
                    $res3 = $row['gramos'];
                    $res4 = $row['nota'];
                    $res5 = $row['id'];
                }
                printf('<input type="hidden" value="%s" class="form-control" name="editid" style="text-align: center;" placeholder="Nombre del producto *" required><br>
                <input type="text" value="%s" class="form-control" name="nombrep" style="text-align: center;" placeholder="Nombre del producto *" required><br>
                <input type="text" value="%s" class="form-control" name="preciop" style="text-align: center;" placeholder="Precio por gramo *" required><br>

                <input type="number" value="%s" class="form-control" name="stockp" style="text-align: center;" placeholder="Stock en gramos: *" step="any" required><br>
                <input type="text" value="%s" class="form-control" name="notap" style="text-align: center;" placeholder="Notas: " ><br>
                <input type="submit" class="btn btn-primary" name="enviar" value="Añadir al inventario">', $res5, $res1, $res2, $res3,$res4); ?>

              </form>

						</div>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
			<footer>
				<div class="container-fluid">
          <p class="copyright">&copy; wControl 2017 - Developed by BlackLeaf (<?php echo $version?>)</p>
				</div>
			</footer>
		</div>
		<!-- END MAIN -->
	</div>
	<!-- END WRAPPER -->
	<!-- Javascript -->
	<script src="assets/js/jquery/jquery-2.1.0.min.js"></script>
	<script src="assets/js/bootstrap/bootstrap.min.js"></script>
	<script src="assets/js/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="assets/js/klorofil.min.js"></script>
</body>

</html>
<?php ob_end_flush(); ?>
