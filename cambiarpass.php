<?php
$webid = 'clientes';
include_once "app/iniciar.php";


if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  $contraactu = $_POST['contraactual'];
  $contraactual = hash('sha512', $contraactu);
  $contranueva = $_POST['contranueva'];
  $contraconf = $_POST['contraconf'];
  $passnew = hash('sha512', $contranueva);

  if($pass == $contraactual){
    if($contranueva == $contraconf){
  $sql = "UPDATE `admin` SET `pass`='$passnew' WHERE user = '$username'";
  $query = mysql_query($sql);
  if ($query === false) {
      echo "Could not successfully run query ($sql) from DB: " . mysql_error();
      exit;
  }
  else {
    $msg = "¡Exito! Su contraseña ha sido modificada correctamente.";
    WControl::EscribirLog("Se ha realizado un cambio de contraseña.");
  }
}
  else{
    $msg = "Las contraseñas no coinciden.";
  }
}
  else{
    $msg = "La contraseña actual no es correcta.";
    WControl::EscribirLog("Se ha intentado realizar un cambio de contraseña pero no se pudo procesar ya que la contraseña actual era incorrecta.");
  }
}
$webid = 'seguridad';
?>

<!doctype html>
<html lang="en">

<head>
	<title>Cambiar contraseña | wControl</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- CSS -->
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/vendor/icon-sets.css">
  <link rel="stylesheet" href="assets/css/<?php echo $color?>">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="assets/css/demo.css">
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
</head>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- SIDEBAR -->
		<div class="sidebar">
			<div class="brand">
        <a href="index.php">
          <center>
            <font color="white">
          <h4><img src="assets/img/logoini.png"<br> <?php echo $nombreclub?></h4>
        </font>
        </center>
        </a>
			</div>
			<div class="sidebar-scroll">
				<nav>
					<?php include "assets/menu.php"; ?>
				</nav>
			</div>

		</div>
		<!-- END SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
			<!-- NAVBAR -->
			<nav class="navbar navbar-default">
				<div class="container-fluid">
					<div class="navbar-btn">
						<button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-arrow-left-circle"></i></button>
					</div>
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-menu">
							<span class="sr-only">Toggle Navigation</span>
							<i class="fa fa-bars icon-nav"></i>
						</button>
					</div>
					<div id="navbar-menu" class="navbar-collapse collapse">

						<ul class="nav navbar-nav navbar-right">


							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="assets/img/user.png" class="img-circle" alt="Avatar"> <span><?php echo $nombreclub?></span> <i class="icon-submenu lnr lnr-chevron-down"></i></a>
								<ul class="dropdown-menu">
									<li><a href="assets/logout.php"><i class="lnr lnr-exit"></i> <span>Salir</span></a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</nav>
			<!-- END NAVBAR -->
			<!-- MAIN CONTENT -->
			<div class="main-content">
        <?php if (isset($msg)) {
          printf('<div class="alert alert-info" role="alert">
  <i class="fa fa-exclamation-circle" aria-hidden="true"></i>
  %s
</div>', $msg);
        } ?>
				<div class="container-fluid">
					<center>

					<h3 class="page-title">Cambiar contraseña<br>
          <small><small><font color="red">Recuerda poner una totalmente segura.</font></small></small></h3>
					<div class="panel panel-headline">
						<div class="panel-body">
              <form action="<?= $_SERVER['PHP_SELF'] ?>" method="post">
                <input type="password" class="form-control" name="contraactual" style="text-align: center;" placeholder="Contraseña actual" required><br>
                <input type="password" class="form-control" name="contranueva" style="text-align: center;" placeholder="Nueva contraseña" required><br>
                <input type="password" class="form-control" name="contraconf" style="text-align: center;" placeholder="Repite la contraseña" required><br>


                <input type="submit" class="btn btn-primary" name="enviar" value="Cambiar contraseña">
              </form>

						</div>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
			<footer>
				<div class="container-fluid">
          <p class="copyright">&copy; wControl 2017 - Developed by BlackLeaf (<?php echo $version?>)</p>
				</div>
			</footer>
		</div>
		<!-- END MAIN -->
	</div>
	<!-- END WRAPPER -->
	<!-- Javascript -->
	<script src="assets/js/jquery/jquery-2.1.0.min.js"></script>
	<script src="assets/js/bootstrap/bootstrap.min.js"></script>
	<script src="assets/js/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="assets/js/klorofil.min.js"></script>
</body>

</html>
