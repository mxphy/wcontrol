<?php
$webid = 'clientes';
include_once "app/iniciar.php";
?>

<!doctype html>
<html lang="en">

<head>
	<title>Editar socio | wControl</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- CSS -->
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/vendor/icon-sets.css">
	<link rel="stylesheet" href="assets/css/<?php echo $color?>">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="assets/css/demo.css">
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
</head>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- SIDEBAR -->
		<div class="sidebar">
			<div class="brand">
				<a href="index.php">
				  <center>
				    <font color="white">
				  <h4><img src="assets/img/logoini.png"<br> <?php echo $nombreclub?></h4>
				</font>
				</center>
				</a>
			</div>
			<div class="sidebar-scroll">
				<nav>
					<?php include "assets/menu.php"; ?>
				</nav>
			</div>

		</div>
		<!-- END SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
			<!-- NAVBAR -->
			<nav class="navbar navbar-default">
				<div class="container-fluid">
					<div class="navbar-btn">
						<button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-arrow-left-circle"></i></button>
					</div>
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-menu">
							<span class="sr-only">Toggle Navigation</span>
							<i class="fa fa-bars icon-nav"></i>
						</button>
					</div>
					<div id="navbar-menu" class="navbar-collapse collapse">

						<ul class="nav navbar-nav navbar-right">


							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="assets/img/user.png" class="img-circle" alt="Avatar"> <span><?php echo $nombreclub?></span> <i class="icon-submenu lnr lnr-chevron-down"></i></a>
								<ul class="dropdown-menu">
									<li><a href="assets/logout.php"><i class="lnr lnr-exit"></i> <span>Salir</span></a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</nav>
			<!-- END NAVBAR -->
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<center>
					<h3 class="page-title">Buscar socio</h3>
					<div class="panel panel-headline">
						<div class="panel-body">
							 
              <?php
    								if(isset($_POST['buscar'])){
    								$nombre = $_POST['nombre'];
    								$sql = "SELECT * FROM socios WHERE nombre = '$nombre'";
    								$query = mysqli_query($lin, $sql);
								    $socioexiste = mysqli_num_rows($query);
								    if ($socioexiste == 0) {
								      $msg = 'El socio introducido no existe en la base de datos.';
								    }
    								while ($row = mysqli_fetch_assoc($query)){
    								printf("<form action='eclientefinal.php' method='post'>		
                   					<input class='form-control' name='nombre' type='hidden' value='%s'><br>
                  					Nº de socio: <input class='form-control' name='socio' type='text' value='%s'><br>
    								Email: <input class='form-control' name='email' type='text' value='%s'><br>
    								Teléfono: <input class='form-control' name='telefono' type='text' value='%s'><br>
    								DNI: <input class='form-control' name='dni' type='text' value='%s'><br>
    								Fecha de nacimiento: <input class='form-control' name='nacimiento' type='text' value='%s'><br>
    								Fecha de inscripción: <input class='form-control' name='inscripcion' type='text' value='%s'><br>
    								Precio pagado: <input class='form-control' name='precio' type='text' value='%s'><br>
                  					Avalador: <input class='form-control' name='avalador' type='text' value='%s'><br>
                   					Dirección: <input class='form-control' name='direccion' type='text' value='%s'><br>
                   					Notas: <input class='form-control' name='notas' type='text' value='%s'><br>
    								<input type='submit' name='Modificar' value='Modificar' class='btn btn-info'>
    								</form>", $row["nombre"], $row["socio"], $row["email"], $row["telefono"], $row["dni"], $row["nacimiento"], $row["inscripcion"], $row["precio"], $row["avalador"], $row["direccion"], $row["notas"]);
    								} 
    								
    								}
    								?>
    								<?php
				                    if (isset($msg)) {
							           wControl::MostrarError($msg);
				                    }
			                   ?>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
			<footer>
				<div class="container-fluid">
					<p class="copyright">&copy; wControl 2017 - Developed by BlackLeaf (<?php echo $version?>)</p>
				</div>
			</footer>
		</div>
		<!-- END MAIN -->
	</div>
	<!-- END WRAPPER -->
	<!-- Javascript -->
	<script src="assets/js/jquery/jquery-2.1.0.min.js"></script>
	<script src="assets/js/bootstrap/bootstrap.min.js"></script>
	<script src="assets/js/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="assets/js/klorofil.min.js"></script>
</body>

</html>
