<?php
$webid = 'ventas';
include_once "app/iniciar.php";

$fecha = WControl::ObtenerFecha();
$sql = "SELECT * FROM ventas WHERE fecha = '$fecha'";
$query = mysql_query($sql);
if ($query === false) {
		echo "<option>Could not successfully run query ($sql) from DB: " . mysql_error();
		echo "</option>";
		exit;
}
$ventahoy = mysql_num_rows($query);
$dinerohoy = 0;
while ($row = mysql_fetch_assoc($query)) {
	$venprecio = $row['preciofinal'];
	$dinerohoy = $dinerohoy + $venprecio;
}

?>

<!doctype html>
<html lang="en">

<head>
	<title>Historial | wControl</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- CSS -->
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/vendor/icon-sets.css">
	<link rel="stylesheet" href="assets/css/<?php echo $color?>">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="assets/css/demo.css">
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
</head>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- SIDEBAR -->
		<div class="sidebar">
			<div class="brand">
				<a href="index.php">
				  <center>
				    <font color="white">
				  <h4><img src="assets/img/logoini.png"<br> <?php echo $nombreclub?></h4>
				</font>
				</center>
				</a>
			</div>
			<div class="sidebar-scroll">
				<nav>
					<?php include "assets/menu.php"; ?>
				</nav>
			</div>

		</div>
		<!-- END SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
			<!-- NAVBAR -->
			<nav class="navbar navbar-default">
				<div class="container-fluid">
					<div class="navbar-btn">
						<button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-arrow-left-circle"></i></button>
					</div>
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-menu">
							<span class="sr-only">Toggle Navigation</span>
							<i class="fa fa-bars icon-nav"></i>
						</button>
					</div>
					<div id="navbar-menu" class="navbar-collapse collapse">

						<ul class="nav navbar-nav navbar-right">


							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="assets/img/user.png" class="img-circle" alt="Avatar"> <span><?php echo $nombreclub?></span> <i class="icon-submenu lnr lnr-chevron-down"></i></a>
								<ul class="dropdown-menu">
									<li><a href="assets/logout.php"><i class="lnr lnr-exit"></i> <span>Salir</span></a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</nav>
			<!-- END NAVBAR -->
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<center>
						<?php if (isset($_GET['action'])) {
							if ($_GET['action'] = "BorrarVenta") {
								echo '<div class="alert alert-info" role="alert">Borraste un registro de venta.</div>';
							}
						}
						?>
					<h3 class="page-title">Historial</h3>
					<div class="panel panel-headline">
						<div class="panel-body">
              <div class="panel panel-default">
								<div class="panel-heading">Lista de ventas</div>
								<table class="table">
								<thead>
								<tr>
								<th>#</th>
								<th>Fecha</th>
								<th>Socio</th>
								<th>Precio final</th>
								<th>Vendedor</th>
								<th>Acciones</th>
								</tr>
								</thead>
								<tbody>
								<?php $sql = "SELECT * FROM ventas order by id desc";
								$query = mysql_query($sql);
								if ($query === false) {
										echo "<option>Could not successfully run query ($sql) from DB: " . mysql_error();
										echo "</option>";
										exit;
								}
								$totalventas = mysql_num_rows($query);
								$totaldinero = 0;
								while ($row = mysql_fetch_assoc($query)) {
									$vnum = $row['id'];
									$vfecha = $row['fecha'];
									$vsocio = $row['socio'];
									$vvendedor = $row['vendedor'];
									$vprecio = $row['preciofinal'];
									$totaldinero = $totaldinero + $vprecio;
									printf('<tr>
										<th scope="row">%s</th>
										<td>%s</td>
										<td>%s</td>
										<td>€ %s</td>
										<td>%s</td>
										<td><a href="venta.php?id=%s" class="btn btn-info"><i class="fa fa-eye"></i></a><a href="borrarventa.php?prod=%s" class="btn btn-danger"><i class="fa fa-trash"></i></a></td>
									</tr>', $vnum, $vfecha, $vsocio, $vprecio, $vvendedor, $vnum, $vnum);
								} ?>
								</tbody>
								</table>

								</div>
								<?php printf('<h4>Hoy: € %s en %s ventas</h4>', $dinerohoy, $ventahoy); ?>
								<?php printf('<h3>Total: € %s en %s ventas</h3>', $totaldinero, $totalventas); ?>
						</div>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
			<footer>
				<div class="container-fluid">
					<p class="copyright">&copy; wControl 2016 - Developed by Muphy (BlackLeaf)</p>
				</div>
			</footer>
		</div>
		<!-- END MAIN -->
	</div>
	<!-- END WRAPPER -->
	<!-- Javascript -->
	<script src="assets/js/jquery/jquery-2.1.0.min.js"></script>
	<script src="assets/js/bootstrap/bootstrap.min.js"></script>
	<script src="assets/js/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="assets/js/klorofil.min.js"></script>
</body>

</html>
