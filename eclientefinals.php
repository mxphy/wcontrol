<?php
$webid = 'clientes';
include_once "app/iniciar.php";
?>

<!doctype html>
<html lang="en">

<head>
	<title>Editar socio | wControl</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- CSS -->
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/vendor/icon-sets.css">
	<link rel="stylesheet" href="assets/css/<?php echo $color?>">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="assets/css/demo.css">
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
</head>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- SIDEBAR -->
		<div class="sidebar">
			<div class="brand">
				<a href="index.php">
				  <center>
				    <font color="white">
				  <h4><img src="assets/img/logoini.png"<br> <?php echo $nombreclub?></h4>
				</font>
				</center>
				</a>
			</div>
			<div class="sidebar-scroll">
				<nav>
					<?php include "assets/menu.php"; ?>
				</nav>
			</div>

		</div>
		<!-- END SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
			<!-- NAVBAR -->
			<nav class="navbar navbar-default">
				<div class="container-fluid">
					<div class="navbar-btn">
						<button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-arrow-left-circle"></i></button>
					</div>
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-menu">
							<span class="sr-only">Toggle Navigation</span>
							<i class="fa fa-bars icon-nav"></i>
						</button>
					</div>
					<div id="navbar-menu" class="navbar-collapse collapse">

						<ul class="nav navbar-nav navbar-right">


							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="assets/img/user.png" class="img-circle" alt="Avatar"> <span><?php echo $nombreclub?></span> <i class="icon-submenu lnr lnr-chevron-down"></i></a>
								<ul class="dropdown-menu">
									<li><a href="assets/logout.php"><i class="lnr lnr-exit"></i> <span>Salir</span></a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</nav>
			<!-- END NAVBAR -->
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<center>
					<h3 class="page-title">Buscar socio</h3>
					<div class="panel panel-headline">
						<div class="panel-body">
              <?php
                  include('assets/scripts/conexpca.php');//incluye el archivo php que contiene la conexion
                  $con=Conectar();//variable que almacena la conexión ala base de datos
                  if(isset($_REQUEST['Modificar'])){

                  $nombre=$_REQUEST['nombre'];
                  $socio=$_REQUEST['socio'];
                  $email=$_REQUEST['email'];
                  $telefono=$_REQUEST['telefono'];
                  $dni=$_REQUEST['dni'];
                  $nacimiento=$_REQUEST['nacimiento'];
                  $inscripcion=$_REQUEST['inscripcion'];
                  $precio=$_REQUEST['precio'];
                  $avalador=$_REQUEST['avalador'];
                  $direccion=$_REQUEST['direccion'];
                  $notas=$_REQUEST['notas'];


                  $queryi="update socios set nombre='$nombre', socio='$socio', email='$email', telefono='$telefono' , dni='$dni', nacimiento='$nacimiento', inscripcion='$inscripcion', precio='$precio', avalador='$avalador', direccion='$direccion', notas='$notas' where socio='$socio'";//consulta sql

                  $val=mysql_query($queryi,$con);//ejecutando consulta

                  if(!$val){
                  echo "<br>No se han podido modificar los datos, intentalo mas tarde.<br> <a href='editarcliente.php'>Regresar</a>";
                  }
                  else {
                  echo "<br>¡Cambios realizados correctamente!<br> <a href='editarcliente.php'>Regresar</a>";
									WControl::EscribirLog("Se ha realizado una edición de usuario por número de socio en el socio número ".$socio."");
                  }
                  }
                  ?>

					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
			<footer>
				<div class="container-fluid">
					<p class="copyright">&copy; wControl 2017 - Developed by BlackLeaf (<?php echo $version?>)</p>
				</div>
			</footer>
		</div>
		<!-- END MAIN -->
	</div>
	<!-- END WRAPPER -->
	<!-- Javascript -->
	<script src="assets/js/jquery/jquery-2.1.0.min.js"></script>
	<script src="assets/js/bootstrap/bootstrap.min.js"></script>
	<script src="assets/js/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="assets/js/klorofil.min.js"></script>
</body>

</html>
