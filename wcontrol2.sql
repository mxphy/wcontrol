-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 09-06-2017 a las 18:54:48
-- Versión del servidor: 10.1.21-MariaDB
-- Versión de PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `wcontrol`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `user` varchar(30) NOT NULL,
  `pass` varchar(30) NOT NULL,
  `nombre` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `admin`
--

INSERT INTO `admin` (`id`, `user`, `pass`, `nombre`) VALUES
(1, 'demo', 'demo', 'demo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gastos`
--

CREATE TABLE `gastos` (
  `id` int(11) NOT NULL,
  `cantidad` varchar(256) NOT NULL,
  `concepto` varchar(256) NOT NULL,
  `fecha` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `gastos`
--

INSERT INTO `gastos` (`id`, `cantidad`, `concepto`, `fecha`) VALUES
(1, '2', 'Nose', '5-6-2017'),
(3, '3', 'asd', '5-6-2017'),
(4, '0.1', 'eeeeesaaaa', '5-6-2017');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inventario`
--

CREATE TABLE `inventario` (
  `id` int(11) NOT NULL,
  `nombre` varchar(256) NOT NULL,
  `gramos` varchar(256) NOT NULL,
  `precio` varchar(50) NOT NULL,
  `nota` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `inventario`
--

INSERT INTO `inventario` (`id`, `nombre`, `gramos`, `precio`, `nota`) VALUES
(4, 'American Pie', '496.3', '7', '60% Sativa, 300-1000 g/planta Alt. 100-300 cm, Flor. 60-70 dÃ­as, THC: 20%, CBD: 0,6%');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prestamos`
--

CREATE TABLE `prestamos` (
  `id` int(11) NOT NULL,
  `hora` varchar(256) NOT NULL,
  `socio` varchar(256) NOT NULL,
  `atencion` varchar(256) NOT NULL,
  `objeto` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `prestamos`
--

INSERT INTO `prestamos` (`id`, `hora`, `socio`, `atencion`, `objeto`) VALUES
(1, '5-6-2017', 'ECM1', 'Ale', 'PS4');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sesiones`
--

CREATE TABLE `sesiones` (
  `id` int(11) NOT NULL,
  `usuario` varchar(256) NOT NULL,
  `sesionid` varchar(500) NOT NULL,
  `hora` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `socios`
--

CREATE TABLE `socios` (
  `id` int(11) NOT NULL,
  `socio` varchar(30) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `telefono` varchar(50) NOT NULL,
  `dni` varchar(50) NOT NULL,
  `nacimiento` varchar(50) NOT NULL,
  `inscripcion` varchar(50) NOT NULL,
  `precio` varchar(50) NOT NULL,
  `avalador` varchar(100) NOT NULL,
  `direccion` varchar(100) NOT NULL,
  `notas` varchar(100) NOT NULL,
  `separacion` varchar(5) NOT NULL DEFAULT '-'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `socios`
--

INSERT INTO `socios` (`id`, `socio`, `nombre`, `email`, `telefono`, `dni`, `nacimiento`, `inscripcion`, `precio`, `avalador`, `direccion`, `notas`, `separacion`) VALUES
(7, 'ECM1', 'Mario Jorge Exposito Carballo', 'mario_er_loko92@hotmail.com', '658337313', '78644566 E', '11/07/92', '13/02/17', '25', 'Arturo Montesinos Reyes', 'C/ Adan Martin Menis 1', 'PAGADO Y COMPLETO', '-'),
(8, 'JE1', 'Erik Johansson', '', '634758310', 'X 9776880 U ', '20/06/1990', '10/02/17', '25', 'Arturo Montesinos Reyes', 'C/ Los Angeles 8-30', 'PAGADO Y COMPLETO', '-'),
(9, 'PRJ1', 'Johan Alexis Puig Rodriguez', 'johanpuig97@gmail.com', '669491130', '46261043Q', '18/09/97', '16/02/17', '25', 'Alejandro Bello Rancel BRA1', 'C/Ernesto Castro Diaz 15 ', 'PAGADO Y COMPLETO', '-'),
(10, 'FBI1', 'Ivan Fernandez Bardanca ', 'ivanfdezbar@hotmail.com', '649198702', '45895883G', '13/03/96', '13/02/17', '25', 'Vitin Thawani ', 'Avda. Constitucion 45', 'PAGADO Y COMPLETO', '-'),
(14, 'MOA1', 'Aaron Morales Ortiz', 'Bryannti23@gmail.com', '628892944', '45895976M', '08/03/1996', '13/02/2017', '25', 'John Cristofher Guema Valdez', 'C/ Barranco de ingles 8', 'PAGADO Y COMPLETO', '-'),
(15, 'RNG1', 'Guillermo Leonardo Rossi Navarro', '', '695292995', '45897867 X', '07/09/1996', '21/02/17', '25', 'Nestor Luis Diaz  Dominguez', 'C/ Mencey Adjona 113', 'PAGADO Y COMPLETO', '-'),
(16, 'DGMA1', 'Miguel Abian Diaz Garcia', 'ese_miguelito.sm@hotmail.com', '', '45854979 W', '04/07/1995', '04/03/17', '25', 'Luis Manuel Hernandez Matie', 'C. General Tamaide RS Maria 7', 'PAGADO Y COMPLETO', '-'),
(17, 'QGV1', 'Victor Antonio Quintero Gonzalez', 'victorglez.15@hotmail.com', '649015203', '45863008 L', '15/12/1995', '21/02/17', '25', 'Antonio Quintero Martin ', 'C. General de San Miguel de Abona 36', 'PAGADO Y COMPLETO', '-'),
(18, 'QMA1', 'Antonio Quintero Martin', '', '672698769', '45896763 X', '10/12/1996', '20/02/17', '25', 'Nestor Luis Diaz Dominguez', 'C. GENERALDEL SUR', 'PAGADO Y COMPLETO', '-'),
(19, 'GGA2', 'Alejandro Garcia Garcia', 'Hockenheim-@hotmail.com', '651840520', '78636485 Z', '08/09/1993', '21/02/17', '25', 'Arturo Montesinos Reyes', 'C/ Veinticinco de Julio 34', 'PAGADO Y COMPLETO', '-'),
(20, 'AAJ1', 'James Armas Alcock', 'djjamesarmas@hotmail.com', '667056966', '45863215 L', '01/05/1994', '17/02/17', '25', 'Michela Butarelli', 'C/ Cascabel Buzanada 57', 'PAGADO Y COMPLETO', '-'),
(21, 'EGE1', 'Emiliano Estevez Gonzalez', 'emitevez@icloud.com', '600 62 00 41', '78 61 32 27 D', '06/04/1984', '17/03/2017', '25', 'Arturo Montesinos Reyes', 'C. Alemania 1 PBJ 0011 Las Americas', 'PAGADO Y COMPLETO', '-'),
(22, 'MAC1', 'Ashley Meijvis', 'ashley.mejivis@hotmail.com', '667 04 07 15', 'Y 10 42 83 8 D ', '22/04/1994', '10/02/217', '25', 'AM1 Mery', 'Camino Fuentes CaÃ±izares La Laguna N4', 'PAGADO Y COMPLETO', '-'),
(23, 'SD1', 'Daniel Seccaroni', '', '605 32 96 71', 'AT 88 71 23 1', '29/05/1984', '10/022017', '25', 'AM 1 Mery', 'Santa Cruz de Tenerife', 'PAGADO Y COMPLETO', '-'),
(24, 'PA1', 'Alessandro Passalacqua', 'alessandro.pass88@gmail.com', '692 39 36 39', 'AX 74 38 95 0 ', '29/09/1988', '14/02/2017', '25', 'AM1 Mery', 'Avenida de la Constitucion 53', 'PAGADO Y COMPLETO', '-'),
(25, 'VE1', 'eros Venturelu', '', '609 17 63 99', 'AU 61 92 40 2', '03/06/1993', '1402/2017', '25', 'AM1 Mery', 'C/ Ensenadas Abades N6', 'PAGADO Y COMPLETO', '-'),
(26, 'TG1', 'Gulshan Kumar Thawani', 'garythawani@gmail.com', '626 10 92 71', 'X 51 31 31 5 S', '29/07/1991', '14/02/2017', '25', 'AM1 Mery', 'Av. Ernesto Sarti APTS. Parque Royal 1', 'PAGADO Y COMPLETO', '-'),
(27, 'TG1', 'Gulshan Kumar Thawani', 'garythawani@gmail.com', '626 10 92 71', 'X 51 31 31 5 S', '29/07/1991', '14/02/2017', '25', 'AM1 Mery', 'Av. Ernesto Sarti APTS. Parque Royal 1', 'PAGADO Y COMPLETO', '-'),
(28, 'bm1', 'Michela Buttarelli', 'mikyansuna@gmail.com', '602521869', 'ar4897720', '18/05/1977', '10/02/2017', '25', 'am1', 'C/Real Los Portelas', 'PAGADO Y COMPLETO', '-'),
(29, 'RF1', 'Federico Re', 'fedefioscmi@liveroit', '656 01 11 46', 'Y 52 10 60 4 Z', '27/08/1992', '10/02/2017', '25', 'Antonio Miguel Glez. Arzola GAMA1', 'Santa Cruz de Tenerife', 'PAGADO Y COMPLETO', '-'),
(30, 'NHF1', 'Henry Fabio Niel', '', '625 66 11 19', 'Y 44 54 85 8 W', '08/12/1992', '10/02/2017', '25', 'BM1', 'Calle Cristo ossuna', 'PAGADO Y COMPLETO', '-'),
(31, 'TL1', 'Luigi Turco', '', '', 'Y 43 88 07 9 S', '03/09/1979', '14/02/2017', '25', 'AM1', 'Los Helechos', 'PAGADO Y COMPLETO', '-'),
(32, 'ML1', 'Luciano Mercanti ', 'lucianomercantil74@gmail.com', '632 54 78 06', 'Y 34 68 38 4 K', '09/09/1974', '16/02/2017', '25', 'MF1', 'Adeje', 'PAGADO Y COMPLETO', '-'),
(33, 'GM1', 'Michele Giors', 'michele.giors@virgilio.it', '603 22 05 99', 'Y 47 79 18 3 G', '21/11/1991', '16/02/17', '25', 'MF1', 'C/ La Marina 10 Los Cristianos ', 'PAGADO Y COMPLETO', '-'),
(34, 'AG1', 'David Agayan Palmanova', '', '626 03 34 98', '43 39 78 9 AA', '14/09/1992', '16/02/17', '25', 'NHF1', 'Vialoeonna 1/1 triesto', 'PAGADO Y COMPLETO', '-'),
(35, 'CFR1', 'Reynier Calderon Fernandez', 'Reycubas@hotmail.it', '605 11 21 87', 'Y 42 53 76 2 H', '20/11/1986', '10/02/2017', '25', 'BM1', 'C/ Santa Rosalia N5 3B Taco La Laguna', 'PAGADO Y COMPLETO', '-'),
(37, 'PBJ1', 'Judyt Pariente Blachuinska', 'Kiomnia1991@gmail.com', '674 13 14 68', '43 20 85 84 W', '28/06/1991', '21/02/17', '25', 'BM1', 'C/ Manuel Bello Ramos 10', 'PAGADO Y COMPLETO', '-'),
(38, 'MG1', 'Gabriele Mancini', 'gmancio87@gmail.com', '654 25 10 73', 'Y 40 17 57 8 M ', '08/05/1987', '10/02/2017', '25', 'Antonio Miguel Glez. Arzola GAMA1', 'C/ Domingo Perez Mink Finca EspaÃ±a 56 P.02', 'PAGADO Y COMPLETO', '-'),
(39, 'PGM1', 'Maria Perdomo Grosso', 'mari_perdo@hotmail.com', '671 14 57 11', '54 06 18 12 J', '26/06/1991', '21/02/17', '25', 'MG1', 'C/Baltasar NuÃ±ez 7', 'PAGADO Y COMPLETO', '-'),
(40, 'MF1', 'Federico Mingiardi', 'cebercocorturio@live.com', '609 12 10 27', 'Y 22 20 07 6 S', '19/10/1987', '10/02/2017', '25', 'AM1', 'C/ Vista MAr Los Abrigos 1 P. 1C', 'PAGADO Y COMPLETO', '-'),
(42, 'AOP1', 'Pablo Alvarez Ortiz', 'pabloalvort92@gmail.com', '653 40 98 18', '78 63 98 18 N', '09/05/1992', '11/03/2017', '25', 'GOA1', 'C/ El Peralito 3', 'PAGADO Y COMPLETO', '-'),
(43, 'FGVH1', 'Victor Hugo Francisco Glez.', 'vh91@live.com', '600 647 063', '78 64 31 26 P', '0902/1991', '10/02/2017', '25', 'Arturo Montesinos Reyes', 'C/ Mecanico 3-1', 'PAGADO Y COMPLETO', '-'),
(44, 'CG1', 'Giuseppe Corte', 'pypz@hotmail.com', '602 55 62 95', 'Y 44 75 46 6 W', '12/01/1980', '18/02/2017', '30', 'AM1', 'C/ La Viranca 33 Arona', 'PAGADO Y COMPLETO', '-'),
(45, 'MG3', 'Giuseppe Marquez', '', '', 'YA 54 64 69 7', '09/12/1970', '09/03/2017', '25', 'AM1', 'Adeje', 'PAGADO Y COMPLETO', '-'),
(46, 'HC1', 'Christian Mamment ', 'chamment@googlemail.com', '695194157', 'Y4956887X', '21/03/1988', '06/03/2017', '25', 'AM1', '534 Caledonia Park,  calle Alemania, San Eugenio, Adeje, 38660', 'PAGADO Y COMPLETO', '-'),
(47, 'PM1', 'Michele Pucci', '', '611344385', 'Y4476100S', '24/09/1983', '10/02/2017', '25', 'AM1', 'Maria Cristo, Ossuna 16', 'PAGADO Y COMPLETO', '-'),
(48, 'GF1', 'Francesca Giordano', 'fra.gio82@yahoo.it', '605528553', 'Y4369076G', '13/11/1982', '10/02/2017', '25', 'BM1', 'calle Jardin Botanico ', 'PAGADO Y COMPLETO', '-'),
(49, 'GOA1', 'Alejandro Gonzalez Oriz ', 'alegonort@gmail.com', '652602522', '78639815D', '25/01/1992', '10/02/2017', '25', 'GAA1', 'calle Taoro 43, La Orotava', 'PAGADO Y COMPLETO', '-'),
(50, 'NM1', 'Matteo Neri', 'nero.mattew@gmail.com', '600079297', 'Y4727237S', '02/04/1992', '13/02/2017', '25', 'AM1', 'Avda Santiago Puig, 5', 'PAGADO Y COMPLETO', '-'),
(51, 'DGP1', 'Paola Delgado Gonzalez ', 'delgadogonzalez.paola@gmail.com', '600059127', '45730265D', '10/09/1996', '17/02/17', '25', 'GVJC1', 'Calle Manuel Bello Ramos', 'PAGADO Y COMPLETO', '-'),
(52, 'GVJC1', 'John Cristofher Guerra Valdes ', 'john_cristofher94@hotmail.com', '634809607', '49514382M', '24/03/94', '12/02/2017', '25', 'GAA1', 'Las Nieves, Adeje 1 A', 'PAGADO Y COMPLETO', '-'),
(53, 'gy1', 'Yan Gerasimenko ', '', '697712726', 'Y2939193Z', '19/04/1986', '25/02/2017', '25', 'GAA1', 'Ballo Ramos 76 s, Adeje', 'PAGADO Y COMPLETO', '-'),
(54, 'II1', 'Irv Indrek', 'indrek.irv@gmail.com', '626890790', 'Y0826321Z', '09/09/1983', '27/02/2017', '25', 'GY1', 'Caledonia Park 64', 'PAGADO Y COMPLETO', '-'),
(55, 'KA1', 'Alina Kurochkina', 'kyro4kina_ali357@mail.ru', '603170432', 'Y3307732R', '04/11/1989', '27/02/2017', '25', 'GY1', 'Galeon de Adeje, cale la Bonda 25 ', 'PAGADO Y COMPLETO', '-'),
(56, 'DCS1', 'Simona di Clemente ', 'graffitaly@gmail.com', '616224609', 'Y2879994V', '16/02/1982', '27/02/2017', '25', 'BM1', 'calle la MontaÃ±a, 1', 'PAGADO Y COMPLETO', '-'),
(57, 'MRA1', 'Arturo Montesino Reyes', 'arturomontesinoreyes@hotmail.es', '616612577', '78646796K', '15/10/1992', '10/02/2017', '25', 'GF1', 'Calle la Morera N3', 'PAGADO Y COMPLETO', '-'),
(58, 'PK1', 'Karan Pataani', 'karan.pataani@hotmail.com', '603298873', 'J3203985', '13/06/1991', '18/02/2017', '25', 'MRA1', 'calle Tenerife, FaÃ±abe Pueblo ', 'PAGADO Y COMPLETO', '-'),
(59, 'RM1', 'Mattia Pagni', '', '', 'Y3657115Z', '21/07/2017', '11/02/2017', '25', 'BM1', 'Taucho', 'PAGADO Y COMPLETO', '-'),
(60, 'PMP1', 'Pedro Nauzet Perez Mendez', 'pedro_adeje@hotmail.com', '67671627', '45733187X', '04/09/2017', '17/02/2017', '25', 'MB1', 'plaza nuestra seÃ±ora del campo 1', 'PAGADO Y COMPLETO', '-'),
(61, 'RA1', 'Andree Raduta', 'tfe.andres@gmail.com', '655020347', 'Y22330242', '14/09/1986', '20/02/2017', '25', 'BM1', 'Avenida MadroÃ±al', 'Falta por pagar 25 / Falta copia NIE', '-'),
(62, 'LML1', 'Leonardo Lagrega Moreira ', '', '672601061', 'Y1889373Y', '18/12/1995', '16/02/2017', '25', 'BRA1', 'calle El Salonito 39 1A', 'Falta por pagar 25  ', '-'),
(63, 'XFL1', 'Lorenzo Xavier de Francisci', 'lorenzoxavier@gmail.com', '697731145', '', '02/03/1999', '02/03/2017', '25', 'CVJ1', 'Torviscas', 'Falta por pagar 25  ', '-'),
(64, 'HMLM1', 'Luis Manuel Hernandez Matiz', 'hluismanuel92@gmail.com', '637880997', '45851918S', '17/11/1992', '02/03/2017', '25', 'MLO1', 'Calle Tamaide 27B', 'Falta por pagar 25  ', '-'),
(65, 'DDO1', 'Diaz Diaz Oscar', '', '682797422', '45940461P', '25/12/1992', '08/03/2017', '25', 'CMM1', 'calle el Laurel', 'Falta por pagar 25  ', '-'),
(66, 'BGT1', 'Tania Bravo Garcia', 'lexalin_96@hotmail.com', '669781480', '45895718T', '06/09/1996', '21/02/2017', '25', 'PBJ1', 'Avda. Constitucion 34 PO 2 B', 'Falta por pagar 25  ', '-'),
(67, 'ALZ1', 'Zeus Alamo Lopez', 'zayin23za@gmail.com', '662548787', '54064933Y', '12/01/1992', '17/02/2017', '25', 'TPA1', 'calle Hercules,4 urb. Drago apt. 81', 'Falta por pagar 25  ', '-'),
(68, 'MMJ1', 'Jose Antonio Marrero Martin', 'tonimarrero@hotmail.com', '642576132', '45854445N', '14/02/1999', '21/02/2017', '25', 'QMA1', 'Las Zocas', 'Falta por pagar 25  ', '-'),
(69, 'OLA1', 'Almudena Ortiz Clemente', 'almugomerita@hotmail.com', '636343725', '43829728P', '04/02/1994', '20/02/2017', '25', 'CMM1', 'Miguel Delgado Calcerrada', 'Falta por pagar 25  ', '-'),
(70, 'CMM1', 'Max Cabrera Molyneux', 'max_7500@hotmail.com', '607232387', 'X2403117P', '09/04/1993', '20/02/2017', '25', 'DDN1', 'Miguel Delgado Calcerrada 15', 'Falta por pagar 25  ', '-'),
(71, 'TDG1', 'Gabriel Torres Delgado', '', '681162054', '45939383B', '26/01/1999', '20/02/2017', '25', 'VHK1', 'calle Alfonso Melia ', '(averiguar si el nombre socio es TDG o TDA) falta pagar 25', '-'),
(72, 'GNJ1', 'Juan Alberto Gonzalez Negrin (Paco)', 'juangleznegrin10@gmail.com', '678976820', '45897577L', '31/12/1992', '20/02/2017', '25', 'TCI1', 'calle la Cruz 104', 'Falta por pagar 25   ', '-'),
(73, 'TCI1', 'Ibrahim Tovali Cabrera', 'ibratca@gmail.com', '672648291', '45732447Y', '30/09/1995', '20/02/2017', '25', 'VHK1', 'Carretera General del Sur 235 El Roque', 'Falta por pagar 25  ', '-'),
(74, 'GGN1', 'Naybet Garcia Garcia', 'naybetgarciag@gmail.com', '637943822', '46981374G', '04/02/1998', '21/02/2017', '25', 'LML1', 'calle CastaÃ±o callejon correa 4', 'Falta por pagar 25  ', '-'),
(75, 'MLO1', 'Olivez Mesa Ledesma ', 'olivezml45@gmail.com', '697408475', '45896794H', '22/07/1997', '20/02/2017', '25', 'TDA1', 'carretera general del sur 40 ', 'Falta por pagar 25  ', '-'),
(76, 'CMA1', 'Alexis Chinea Montesino', 'alexis_cm_tf@hotmail.com', '649772292', '45980430A', '17/07/1994', '21/02/2017', '25', 'RMM1', 'calle la tostonera 3', 'Falta por pagar 25  ', '-'),
(77, 'MVK1', 'Kevin Mayorgas', 'mayorgaskevin@gmail.com', '', '49513107H', '14/10/1998', '08/03/2017', '25', 'FBI1', 'Las SabadeÃ±as Piso 1 bloque 1, Adeje', 'Falta por pagar 25  ', '-'),
(78, 'LMR2', 'Rayco Lopez Morales ', 'turboelclandestino@gmail.com', '667342767', '45730826H', '03/05/1986', '08/03/2017', '25', 'BF1', 'urb. Santa Ursula Bloque 1 Piso 2C, Los Olivos Adeje ', 'Falta por pagar 25  ', '-'),
(79, 'HGJ2', 'Jairo Hernandez Gonzalez', 'jairohernandezgonzalez@gmail.com', '690964176', '45897950R', '24/06/1995', '28/02/2017', '25', 'GRCJ1', 'Calle Lirio', 'Falta por pagar 25  ', '-'),
(80, 'CLA1', 'Alejandro Casas Lopez', 'alex_casas_lopez@hotmail.com', '636447147', '45899740C', '28/12/1995', '26/02/2017', '25', 'VGD1', 'calle VolcÃ¡n Erosi, San Cristobal de La Laguna', 'Falta por pagar 25  ', '-'),
(81, 'VGN1', 'Nicolas Ma Varela Garcia', 'nicolasmanuel_93@hotmail.com', '656353431', '79071052L', '12/11/1993', '27/02/2017', '25', 'PPAT1', 'Calle San Sebastian 26', 'Falta por pagar 25 / Falta firma avalador ', '-'),
(82, 'MGD1', 'Daniel Ventura Gonzalez', 'danituven@gmail.com', '667080122', '45863111F', '04/05/1993', '26/02/2017', '25', 'GVJC1', 'calle la Botavava Adeje ', 'Falta por pagar 25  ', '-'),
(83, 'BCS1', 'Stefan Bacque Campo', '', '600740467', '45853198F', '01/03/1995', '27/02/2017', '25', 'PPAT1', 'urb. Balcon del Atlantico C3-8', 'Falta por pagar 25 / Falta firma avalador ', '-'),
(84, 'MRG2', 'Giovanni Morales Rosamina ', 'giovi.caco@hotmail.com', '600487945', 'X4550975B', '25/11/1995', '28/02/2017', '25', 'GVJC1', 'calle Manuel delle Ramos 64 1B', 'Falta por pagar 25 / Falta copia NIE', '-'),
(85, 'ACA2', 'Airam Armas Cabrera', 'airam.sm_96@hotmail.com', '689931042', '45863061A', '15/05/1996', '01/03/2017', '25', 'DDNL1', 'carretera General del Sur, San Miguel de Abona', 'Falta por pagar 25  ', '-'),
(86, 'MMA2', 'Antonio Marquez Martin', 'tonymm594@gmail.com', '627389094', '45940661R', '27/11/1998', '02/03/2017', '25', 'CVJ1', 'Adeje', 'Falta por pagar 25 / Falta firma avalador ', '-'),
(87, 'TGA1', 'Aitor Toro Gonzalez', 'aitor_toro@hotmail.com', '608205237', '45863565R', '21/05/1996', '27/02/2017', '25', 'GPP1', 'calle Guirquin 39, Adeje', 'Falta por pagar 25  ', '-'),
(88, 'LGJ1', 'Jesus Adrian Lopez Garcia', 'alardemo@gmail.com', '634238289', '78647972R', '14/05/1992', '21/02/2017', '25', 'MRA1', 'Calle Rosario S. Ed. San Felipe 21', 'Falta por pagar 25  ', '-'),
(89, 'TBA1', 'Adrian Toledo  Brito', 'adriantoledobrito@gmail.com', '674705431', '45867851D', '10/11/1996', '18/02/2017', '25', 'TKN1', 'calle Noruega 30 - Los Cristianos ', 'Falta por pagar 25  ', '-'),
(90, 'PPAT', 'Andres Tanasu Perez Perez', 'andrespererztenerife@hotmail.com', '6588048561', '45895872Q', '12/10/1994', '17/02/2017', '25', 'MRA1', 'calle Cardon S/N ed. Fayal P B 22', 'Falta por pagar 25  ', '-'),
(91, 'PGS1', 'Sara Pacheco Gonzalez', 'pachecogonzalezsara@gmail.com', '656607784', '43384937Z', '03/12/1995', '17/02/2017', '25', 'GAA1', 'calle Gral Las CaÃ±adas', 'PAGADO Y COMPLETO', '-'),
(92, 'PBN1', 'Nicolas Anibal Pacheco Brito', 'niico10pb@gmail.com', '630392060', '43378563B', '10/05/1993', '17/02/2017', '25', 'GAA1', 'calle Zocatin 20  La Orotava', 'PAGADO Y COMPLETO', '-'),
(93, 'FPJM1', 'Jose Mauel Ferando Pombo', '', '661145089', '42264049P', '06/08/1997', '26/02/2017', '25', 'GVJC1', 'Avda La ConstituciÃ³n', 'Falta por pagar 25  ', '-'),
(94, 'BSR1', 'Sawannah Rose Bird', 'srbird17@idad.com', '646452210', 'X2342538B', '17/10/2017', '26/02/2017', '25', 'EMR1', 'c/ Nicolosa 22 Los Menores', 'Falta por pagar 25  ', '-'),
(95, 'HBLL1', 'Leng Lenin Herreria Briores', 'mastrorvlcns@gmail.com', '664556650', 'X71694991T', '10/11/2017', '26/02/2017', '25', 'BM1', 'Adeje', 'Falta por pagar 25  ', '-'),
(96, 'MLAM1', 'Antonio Miguel Martin Leon', 'amartinl100@gmail.com', '648642417', '45896421J', '06/03/2017', '26/02/2017', '25', 'FVJ1', 'c/ Chacacharte 2, Valle San Lorenzo ', 'Falta por pagar 25  ', '-'),
(97, 'BM2', 'Matteo Bocchetti ', '', '604383545', 'Y4537010K', '15/12/1985', '26/02/2017', '25', 'RSB1', 'c/ Nicolosa Los Menores', 'Falta por pagar 25 / rectificar numero socio (dee BM1 a BM2)', '-'),
(98, 'BF1', 'Federico Battiloro', 'fede_barcelones@hotmail.it', '', 'Y0644075C', '26/06/1997', '26/02/2017', '25', 'DCTP1', 'calle Isla de Tenerife', 'Falta por pagar 25 / Falta copia NIE', '-'),
(99, 'MMV1', 'Victor Mouttet Houguet', 'kanariocs@gmail.com', '637079415', '44488060B', '08/12/1992', '26/02/2017', '25', 'DGP1', 'calle 8 Marzo B 42', 'Falta por pagar 25  ', '-'),
(100, 'FSA1', 'Alejandro de La Funente Shuvalov', 'alejandro.raduga@gmail.com', '679660161', '78645604W', '14/10/1991', '26/02/2017', '25', 'GAA1', 'c/ Rosa 7, urb. San Jose', 'Falta por pagar 25  ', '-'),
(101, 'MGL1', 'Loic Martin Gomes', 'loicmartindeoliveira@gmail.com', '682499040', '45850408T', '15/06/1995', '26/02/2017', '25', 'AGO1', 'c/ Las Jarcias ed. Goleta 40 23 ', 'Falta por pagar 25  ', '-'),
(102, 'MRJD1', 'Juan Daniel Mesa Robles ', 'juandabarca@gmail.com', '600873720', '45982009H', '01/11/1996', '26/02/2017', '25', 'GDP1', 'Las Nieves, Adeje', 'Falta por pagar 25  ', '-'),
(103, 'RHA1', 'Alejandro Reyes Hernandez', 'aleejandroreyeshdez@gmail.com', '653741273', '45863776M', '25/12/1996', '11/02/2017', '25', 'GAA1', 'Santiago Fumero 6, Valle San Lorenzo - Arona', 'Falta por pagar 25  ', '-'),
(104, 'PLD1', 'Padilla Linares Daniel ', '', '679566439', '45733167J', '05/01/1993', '25/02/2017', '25', 'PPAT1', 'Arona', 'Falta por pagar 25  ', '-'),
(105, 'GBCJ1', 'Christian Jordan Granados Beltran', '', '', '44512536E', '15/08/1995', '26/02/2017', '25', 'FVJP1', 'Torviscas Alto', 'Falta por pagar 25  ', '-'),
(106, 'FVJP1', 'Juan Pablo Frias Vilahomat', 'jpfrias@gmail.com', '672183959', '45851077W', '21/10/2017', '26/02/2017', '25', 'GVJ1', 'c/ Baleares 8, Torviscas', 'Falta por pagar 20 perdiÃ³ el carnet 1 vez.', '-'),
(107, 'MJS1', 'Sebastian Moyano Johnson', 'sebas96moy@gmail.com', '609221834', 'X9245822Y', '08/10/1996', '04/03/17', '25', 'BF1', 'edif. La Serena - Las Chafiras', 'Pagado solo 5', '-'),
(108, 'PGO1', 'Oscar Pinto Guerrero', 'oscarpintorguerrero@gmail.com', '652349793', '44431107Y', '21/04/1977', '09/03/2017', '25', 'HHG1', 'c7 Tinguaro 16', 'Falta por pagar 25  ', '-'),
(109, 'FTM1', 'Mario Fontalba Talavera ', '', '637234438', '28636542R', '18/01/1979', '09/03/2017', '30', 'PGO2', 'c/ Estrella del Mar', 'falta pagar 30 / Modificar DNI en formulario!!', '-'),
(110, 'TI1', 'Ilenia Turi', 'incturi@gmail.com', '677640397', 'Y3645287P', '29/02/1992', '01/03/2017', '25', 'BM1', 'c/ Guirguin 39 - Las Torres - Adeje', 'Falta por pagar 25  ', '-'),
(111, 'BDR1', 'Roberto Bello Duarte', '', '659158169', '45983418R', '14/08/1998', '03/03/2017', '25', 'PHA1', 'c/ El Lomito 5', 'Falta por pagar 25  ', '-'),
(112, 'DDN1', 'Nestor Luis Diaz Dominguez', 'nestorcheone@icloud.com', '619843722', '45863909T', '06/02/1998', '20/02/2017', '25', 'AM1', 'C/ La Deseada 55 , San Miguel', 'Falta por pagar 25  ', '-'),
(113, 'HHG1', 'Giovanni Hernandez Hernandez ', 'giona_adeje@hotmail.com', '', '45940528Y', '18/12/1996', '26/02/2017', '25', 'GAA1', 'c/ Beheharo', 'Falta por pagar 25  ', '-'),
(114, 'MGV1', 'Victoria Marin Granadillos', 'victoria010397@gmail.com', '673662137', '49947875V', '01/03/1997', '05/03/2017', '25', 'HMLM1', 'calle Lomo de La Fuente 10, Piso 1 - Guaza', 'Falta por pagar 25  ', '-'),
(115, 'GGW1', 'Williams Galindo Garcia', 'wandm@gmail.com', '660923412', '54050927F', '01/12/1996', '19/02/2017', '25', 'GVDC1', 'c/ Calamar 5, 1B - Los Abrigos', 'Falta por pagar 25  ', '-'),
(116, 'HRM1', 'Michael Hernandez Rodriguez', 'princeflow27@gmail.com', '656647298', '79081870G', '16/08/1996', '14/02/2017', '25', 'TBA1', 'Avda. Eugencio Dominguez, Pueblo Canario', 'Falta por pagar 25  ', '-'),
(117, 'TMJ1', 'Joshua Trujillo Medenz', 'joshmendez94@gmail.com', '639505036', '45895836A', '18/04/1994', '19/02/2017', '25', 'TBA1', 'c/ Lujan Perez 2 , 11 ', 'Falta por pagar 25  ', '-'),
(118, 'PRC1', 'Carlos Jose Perez Ramirez', '', '628539049', '45866486R', '17/09/1993', '06/03/2017', '25', 'APAT1', 'FaÃ±abe - Adeje', 'Falta por pagar 25  ', '-'),
(119, 'CGD1', 'Daniel Cornejo Gonzalez ', 'danielcg@hotmail.com', '687677661', '45865986F', '09/08/1995', '04/03/17', '25', 'GNJ1', 'c/ Juan Polido 6 - San Miguel de Abona', 'Falta por pagar 25  ', '-'),
(120, 'GGD1', 'Daniel Garcia Gonzalez', '', '615135592', '46261203S', '16/01/1998', '06/03/2017', '25', 'PPAT1', 'c/ El Drago edf. lagos de Miraverde 529 Adeje', 'Falta por pagar 25  ', '-'),
(121, 'KA2', 'Andreas Karlsson', 'svartaflamman@gmail.com', '622425195', 'Y4563340Q', '10/03/1992', '21/02/2017', '25', 'MRE1', 'c/ Italia Island Village apt. 517 B', 'Falta por pagar 25  ', '-'),
(122, 'LCC1', 'Cristian Linares Castillo ', 'cristianlinarescastillo@gmail.com', '689909777', '45939117K', '13/10/1998', '10/03/2017', '25', 'LML1', 'Carretera General de Arona 65', 'Falta por pagar 25  ', '-'),
(123, 'BMJ1', 'Yonatan Barroso Moleiro', 'jonathanbtf@gmail.com', '653593513', '45726366C', '16/03/2017', '04/03/2017', '25', 'KA2', 'Las Flores 7 - Valle San Lorenzo ', 'Falta por pagar 25  ', '-'),
(124, 'PJ1', 'Joakim Patrik Herrstrom', '', '', 'X3568383W', '19/12/1997', '01/03/2017', '25', 'APAT1', 'Adeje Miraverde', 'Falta por pagar 25  ', '-'),
(125, 'SSL-1', 'Ledicia Mirella San Miguel Saiz', 'Mireya210308@gmail.com', '681014758', '46297576W', '21/03/1998', '26/02/17', '50', 'Arturo Montesinos Reyes', 'Callao Salvage, Callado Beach Apt 805', 'Falta Pagar', '-'),
(126, 'MSA-1', 'Alejandro MartÃ­n SÃ¡mchez', 'unoahiale@gmail.com', '609834223', '78645675-G', '8/02/1994', '23/02/2017', '50', 'Arturo Montesinos Reyes', 'Calle Finlandia, nÂº13 Urb. las Cuevas, La ', '', '-'),
(127, 'MSA-1', 'Alejandro MartÃ­n SÃ¡mchez', 'unoahiale@gmail.com', '609834223', '78645675-G', '8/02/1994', '23/02/2017', '50', 'Arturo Montesinos Reyes', 'Calle Finlandia, nÂº13 Urb. las Cuevas, La ', '', '-'),
(128, 'RRA-1', 'Alejandro Rodriguez Romero', 'pquinterov07@gmail.com', '663911935', '45981292-Z', '08/02/1998', '28/02/17', '0', 'Antonio Quintero MartÃ­n', 'San Miguel de Abona', 'Falta Pagar y Fotocopia del DNI.', '-'),
(129, 'CRCJ-1', 'Carlos Javier GarcÃ­a Romero ', '', '636993652', '45984170-V', '06/08/1994', '24/02/2017', '0', 'Arturo Montesinos Reyes', 'Avnd. MadroÃ±al nÂº4 Urb. Encinas Pt 4.', 'Falta Pagar y Fotocopia del DNI.', '-'),
(130, 'CG-1', 'Giulia Castelli', 'giulia.castelli0693@gmail.com', '600079297', '14727220-K', '06/03/1993', '14/02/2017', '0', 'Mateo Neri', 'Avnd. Santiago Puig, 5.', 'Falta Pagar y Fotocopia del DNI.', '-'),
(131, 'CHI-1', 'MarÃ­a Isabel Hernandez Coello', 'mari_ta@hotmail.com', '631377483', '78404430-Y', '26/06/1972', '9/03/2017', '25', 'Arturo Montesinos Reyes', 'C/Gran Canaria nÂº106', 'Falta Pagar', '-'),
(132, 'TD-1', 'Diana Tambosso', '', '603848758', 'AU-3852496', '06/04/1984', '10/03/2017', '25', 'David Agayan', 'Ctgr Los ', '', '-'),
(133, 'TD-1', 'Diana Tambosso', '', '603848758', 'AU-3852496', '06/04/1984', '10/03/2017', '25', 'David Agayan', 'Ctgr Los Menores, Taucho 72', 'Falta Pagar y Rehacer  Fotocopia del DNI.', '-'),
(134, 'JGMS-1', 'MarÃ­a Soledad Juarez Grillo ', 'SoledadJuarezGrillo@gmail.com', '630380370', '78644629Q', '05/07/1991', '24/02/2017', '50', 'AndrÃ©s TanausÃº PÃ©rez PÃ©rez', 'C/San Gregorio nÂº3 San JosÃ© de Los Llanos, El Tanque.', 'Falta Pagar y Fotocopia del DNI.', '-'),
(135, 'GFI-1', 'IvÃ¡n GonzÃ¡les Felipe', 'ivanglezfelipe@hotmail.com', '639772461', '45863045-X', '13/04/1996', '25/02/2017', '50', 'Alejandro Reyes HernÃ¡ndez', 'Av. San Diego 16', 'Todo Pago', '-'),
(136, 'JJDM-1', 'Jonathan JÃ©sus DomÃ­ngues MartÃ­n', 'jony_adeje88@hotmail.es', '636845815', '45731234-N', '23/03/1988', '19/03/2017', '50', 'Antonio Miguel Glez. Arzola', 'C/La Paloma 5', 'No Paga', '-'),
(137, 'AHI-1', 'IvÃ¡n Abreu Herrera', 'ivanabreuherrera@hotmail.com', '671820469', '45854222-L', '30/06/1996', '19/03/2017', '0', 'Airam Armas Cabrera', 'C/ Erik Lionel Fox nÂº4 P1', 'Falta Pagar', '-'),
(138, 'GSJ-1', 'Joaquin Gonzales Sancho', '', '645111925', '46384015-F', '24/10/1997', '28/03/2017', '50', 'Arturo Montesinos Reyes', 'La Laguna, Barriada city.', 'No Paga', '-'),
(139, 'ESV-1', 'MarÃ­a Victoria Trenco Sanroman', 'marvictreson@gmail.com', '629907862', '45899965-S', '22/06/1994', '29/03/2017', '50', 'Arturo Montesinos Reyes', 'C/emoda n1 - Bajo D', 'No Paga', '-'),
(140, 'PRR-1', 'Rayco Perez Rodriguez', '', '629907862', '78625452-k', '14/10/1984', '29/03/2017', '50', 'Arturo Montesinos Reyes', 'C/Benitez nÂº21/Malpaso', 'No Paga falta renovar fotocopia de dni', '-'),
(141, 'FMA-1', 'Ania Fernandez MiguÃ©ns', 'aniafernandez8@hotmail.com', '617833809', '45940998-Q', '27/30/1997', '11/03/2017', '25', 'Aitor Toro Gonzales', 'Avnd. ContituciÃ³n 27', 'falta pagar 25', '-'),
(142, 'DRA-1', 'Alba Duarte RomÃ¡n', 'albayjordan14@gmail.com', '649043807', '45941924-E', '4/09/1998', '0/0/0', '0', 'JesÃºs Pastigo Mayorgas', 'CallejÃ³n el Fayal', 'Falta fecha de inscripcciÃ³n y pago', '-'),
(143, 'CP-2', 'Paolo Carlesso', 'PablitoPaolo@hotmail.com', '680151017', 'X776319K', '19/10/1984', '10/03/2017', '0', 'Alessandro Passalaqua', 'Calle Venezuela 8', 'Falta pagar y Rehacer dni', '-'),
(144, 'BR-1', 'Roberta Bartocci', 'robertabartocci@hotmail.com', '642691914', 'Y0197426P', '12/12/85', '12/03/2017', '25', 'Alessandro Passalaqua', 'Calle Venezuela 8', 'Falta Pagar y Rehacer  Fotocopia del DNI.', '-'),
(145, 'TS-1', 'Silvia Tisi', 'silviatisi13@gmail.com', '634133340', 'Y4182431', '13/03/1990', '03/03/2017', '50', 'Antonio Miguel Glez. Arzola', 'Calle Taoro, 43 Cruz de la Cebolla, 38300. La Orotava', 'No Paga', '-'),
(146, 'MF-2', 'Michela Fiini', 'miki_85@live.it', '631788153', 'Y4623063-P', '04/05/85', '09/03/2017', '25', 'Arturo Montesinos Reyes', 'Calle pino del aire 62. Chio.', 'Rehacer copia de dni. paga 25', '-'),
(147, 'AR-1', 'Richards Ancs', 'fantujke@gmail.com', '661114763', 'Y1732033-D', '01/04/1998', '10/03/2017', '25', 'Federico Batticoro', 'Torviscas alto, Calle Murcia, Ed Roque del Conde UD 62', 'Faltan 15 â‚¬', '-'),
(148, 'DQ-2', 'Dimitri Quintanilla', 'dimitridelaflor@gmail.com', '646129439', 'X8242974G', '9/7/1994', '31/03/2017', '0', 'Joshua Trujillo  MÃ©ndez', 'C/Guayater 23', 'Falta pagar 50 y Fotocopia del DNI', '-'),
(149, 'GGA-1', 'Alejandro Gonzales Gonzales', 'alex_97_gahri@hotmail.com', '609719175', '45708707-J', '09/08/1997', '19/03/2017', '0', 'Nico Rahala', 'C/Pablo VI NÂº15', 'Falta Pagar y Fotocopia del DNI', '-'),
(150, 'RA-1', 'Alec Rauhala', 'Alecderolder@gmail.com', '687352438', 'X4473369F', '20/08/1994', '19/03/2017', '0', 'Nico Rauhala', 'C/Formentera nÂº6', 'Falta Pagar y nÂº de socio del avalador', '-'),
(151, 'AXV-1', 'Victoria AlemÃ¡n XicarÃ¡', 'victoriahelston96@hotmail.com', '606164382', 'X3068046F', '24/05/1996', '17/03/2017', '0', 'Tania Bravo GarcÃ­a', 'C/Timanfaya, Las Vistas, 9, nÂº5. Chayofa.', 'Falta pagar 50 y Fotocopia del DNI', '-'),
(152, 'GGD-2', 'Deave GonzÃ¡les Gramados', 'deavegonzalesg@hotmail.com', '666996452', '46244983-X', '11/06/1996', '17/03/2017', '0', 'Tania Bravo GarcÃ­a', 'C/ 30 de Mayo edif. Aerama.', 'Falta pagar 50 y Fotocopia del DNI', '-'),
(153, 'PHA-1', 'AdriÃ¡n Placencia HernÃ¡ndez ', 'placenciahernandez97@gmail.com', '648454725', '45867124-H', '11/08/1997', '18/02/2017', '25', 'Antonio Miguel Glez. Arzola GAA1', 'C/ Cardillo NÂº12 Buzanada', 'Falta Pagar 25', '-'),
(154, 'RMM-1', 'Marko Rojas Marichal', 'MRM.IESC@gmail.com', '667310400', '46246593-X', '02/10/1996', '18/02/2017', '25', 'Antonio Miguel Glez. Arzola GAA1', 'Valle San Lorenzo', 'Falta Pagar 25', '-'),
(155, 'NCS-1', 'Samuel Navarro Chell', 'samuelnavarrodist@gmail.com', '677092308', '45852309-S', '19/10/1996', '7/03/2017', '25', 'Juan Daniel Mesa Robles', 'Playa Paraiso C/ La Vera nÂº1', 'Falta por pagar 25  ', '-'),
(156, 'MSA-1', 'Alejandro Mascherin Santoyo', 'moschevin36@gmail.com', '651311455', 'X9688774-R', '27/09/1997', '19/03/2017', '0', 'John Cristopher Guerra', 'Tiforo Bajo C/ La Pedrera nÂº24', 'Falta Pagar', '-'),
(157, 'GPR-2', 'RaÃºl GarcÃ­a PÃ©rez', '', '68424766', '45984730-W', '11/12/1996', '13/04/2017', '0', 'Luis JosÃ© Ramos Portela', 'Adeje', 'Falta pagar y numero real de telefono.', '-'),
(158, 'GF1', 'Francesca Giordano', 'fra.gio82@yahoo.it', '605528553', 'Y4369076G', '13/11/1982', '10/02/2017', '25', 'BM1', 'calle Jardin Botanico ', 'PAGADO Y COMPLETO', '-'),
(161, 'ASD-1', 'ASD', 'a@asd.com', '115642', '44656525A', '07/07/1996', '25/05/2017', '5000', 'Tu mama', 'La concha de la lora 123', 'asd', '-'),
(162, 'GF1', 'ASD', 'alejandrofer116@gmail.com', '12312321', '12312321A', '11/11/1111', '11/11/1111', '233', 'ASD', 'ASD', 'ASD', '-');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventas`
--

CREATE TABLE `ventas` (
  `id` int(11) NOT NULL,
  `producto0` int(11) NOT NULL,
  `cantidad0` varchar(256) NOT NULL,
  `socio` varchar(11) NOT NULL,
  `preciofinal` varchar(256) NOT NULL,
  `producto1` int(11) NOT NULL,
  `cantidad1` varchar(256) NOT NULL,
  `producto2` int(11) NOT NULL,
  `cantidad2` varchar(256) NOT NULL,
  `producto3` int(11) NOT NULL,
  `cantidad3` varchar(256) NOT NULL,
  `producto4` int(11) NOT NULL,
  `cantidad4` varchar(256) NOT NULL,
  `producto5` int(11) NOT NULL,
  `cantidad5` varchar(256) NOT NULL,
  `fecha` varchar(256) NOT NULL,
  `vendedor` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ventas`
--

INSERT INTO `ventas` (`id`, `producto0`, `cantidad0`, `socio`, `preciofinal`, `producto1`, `cantidad1`, `producto2`, `cantidad2`, `producto3`, `cantidad3`, `producto4`, `cantidad4`, `producto5`, `cantidad5`, `fecha`, `vendedor`) VALUES
(19, 4, '0.5', 'ASD-1', '14', 4, '1.5', -1, '0', -1, '0', -1, '0', -1, '0', '22-5-2017', ''),
(20, 4, '0.1', 'QGV1', '4.2', 4, '0.5', -1, '0', -1, '0', -1, '0', -1, '0', '5-6-2017', 'Ale BP');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `gastos`
--
ALTER TABLE `gastos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `inventario`
--
ALTER TABLE `inventario`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `prestamos`
--
ALTER TABLE `prestamos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sesiones`
--
ALTER TABLE `sesiones`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `socios`
--
ALTER TABLE `socios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `gastos`
--
ALTER TABLE `gastos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `inventario`
--
ALTER TABLE `inventario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `prestamos`
--
ALTER TABLE `prestamos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `sesiones`
--
ALTER TABLE `sesiones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `socios`
--
ALTER TABLE `socios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=163;
--
-- AUTO_INCREMENT de la tabla `ventas`
--
ALTER TABLE `ventas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
