<?php
$webid = 'clientes';
include_once "app/iniciar.php";

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
   WControl::AgregarCliente($_POST['nacimiento'], $_POST['nombre'], $_POST['email'], $_POST['telefono'], $_POST['dni'], $_POST['inscripcion'], $_POST['precio'], $_POST['avalador'], $_POST['direccion'], $_POST['notas'], $_POST['socio'], $lin);
}
?>

<!doctype html>
<html lang="en">

<head>
	<title>Agregar socio | wControl</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- CSS -->
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/vendor/icon-sets.css">
  <link rel="stylesheet" href="assets/css/<?php echo $color?>">
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
</head>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- SIDEBAR -->
		<div class="sidebar">
			<div class="brand">
        <a href="index.php">
					<center>
						<font color="white">
					<h4><img src="assets/img/logoini.png"<br> <?php echo $nombreclub?></h4>
				</font>
				</center>
			</a>
			</div>
			<div class="sidebar-scroll">
				<nav>
					<?php include "assets/menu.php"; ?>
				</nav>
			</div>

		</div>
		<!-- END SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
			<!-- NAVBAR -->
			<nav class="navbar navbar-default">
				<div class="container-fluid">
					<div class="navbar-btn">
						<button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-arrow-left-circle"></i></button>
					</div>
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-menu">
							<span class="sr-only">Toggle Navigation</span>
							<i class="fa fa-bars icon-nav"></i>
						</button>
					</div>
					<div id="navbar-menu" class="navbar-collapse collapse">

						<ul class="nav navbar-nav navbar-right">


							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="assets/img/user.png" class="img-circle" alt="Avatar"> <span><?php echo $nombreclub?></span> <i class="icon-submenu lnr lnr-chevron-down"></i></a>
								<ul class="dropdown-menu">
									<li><a href="assets/logout.php"><i class="lnr lnr-exit"></i> <span>Salir</span></a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</nav>
			<!-- END NAVBAR -->
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<center>
            <?php if (isset($mensaje)) {
              printf('<div class="alert alert-info" role="alert">

      %s
    </div>', $mensaje);
            }?>
            <?php if (isset($msgerror)) {
              printf('<div class="alert alert-danger" role="alert">

      Error: %s
    </div>', $msgerror);
            } ?>
					<h3 class="page-title">Registrar nuevo socio<br>
          <small><small><font color="red">Todos los campos con * son obligatorios</font></small></small></h3>
					<div class="panel panel-headline">
						<div class="panel-body">
              <form action="<?= $_SERVER['PHP_SELF'] ?>" method="post">
                <input type="text" class="form-control" name="socio" style="text-align: center;" placeholder="Nº de socio *" required><br>
                <input type="text" class="form-control" name="nombre" style="text-align: center;" placeholder="Nombre y apellidos *" required><br>
                <input type="text" class="form-control" name="email" style="text-align: center;" placeholder="Correo electrónico"><br>
                <input type="text" class="form-control" name="telefono" style="text-align: center;" placeholder="Número de telefono"><br>
                <input type="text" class="form-control" name="dni" style="text-align: center;" placeholder="DNI"><br>
                <input type="text" class="form-control" name="nacimiento" style="text-align: center;" placeholder="Fecha de nacimiento (DD-MM-AAAA) *" required><br>
                <input type="text" class="form-control" name="inscripcion" style="text-align: center;" placeholder="Fecha de inscripción *" required><br>
                <input type="text" class="form-control" name="precio" style="text-align: center;" placeholder="Precio por subscripción *" required><br>
                <input type="text" class="form-control" name="avalador" style="text-align: center;" placeholder="Avalador: *" required><br>
                <input type="text" class="form-control" name="direccion" style="text-align: center;" placeholder="Dirección: *" required><br>
                <input type="text" class="form-control" name="notas" style="text-align: center;" placeholder="Notas: " ><br>


                <input type="submit" class="btn btn-primary" name="enviar" value="Registrar nuevo socio">
              </form>

						</div>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
			<footer>
				<div class="container-fluid">
					<p class="copyright">&copy; wControl 2017 - Developed by BlackLeaf (<?php echo $version?>)</p>
				</div>
			</footer>
		</div>
		<!-- END MAIN -->
	</div>
	<!-- END WRAPPER -->
	<!-- Javascript -->
	<script src="assets/js/jquery/jquery-2.1.0.min.js"></script>
	<script src="assets/js/bootstrap/bootstrap.min.js"></script>
	<script src="assets/js/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="assets/js/klorofil.min.js"></script>
</body>

</html>
