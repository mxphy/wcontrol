<ul class="nav">
  <li><form method="POST" action="bclientensocio.php">
  <input type="text" class="form-control" name="busqueda" size="1">
  <center><input type="submit" class="btn btn-primary" value="Buscar" name="buscar"></center></li><br>
  </form>
  <?php if (isset($webid)) {
    $p1 = ' ';
    $p2 = ' ';
    $p3 = ' ';
    $p4 = ' ';
    $p5 = ' ';
    $p6 = ' ';
    if ($webid == 'inicio') {
      $p1 = 'active';
    }
    if ($webid == 'clientes') {
      $p2 = 'active';
    }
    if ($webid == 'inventario') {
      $p3 = 'active';
    }
    if ($webid == 'ventas') {
      $p4 = 'active';
    }
    if ($webid == 'prestamos') {
      $p5 = 'active';
    }
    if ($webid == 'seguridad') {
      $p6 = 'active';
    }
    printf('<li><a href="index.php" class="%s"><i class="lnr lnr-home"></i> <span>Inicio</span></a></li>
    <li>
      <a href="#clientes"  data-toggle="collapse" class="collapsed %s"><i class="fa fa-users"></i> <span>Socios</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
      <div id="clientes" class="collapse ">
        <ul class="nav">
          <li><a href="addcliente.php" class="">Añadir socio</a></li>
          <li><a href="buscarcliente.php" class="">Buscar socio</a></li>
          <li><a href="editarcliente.php" class="">Editar socio</a></li>
        </ul>
      </div>
    </li>
    <li>
      <a href="#inventario" data-toggle="collapse" class="collapsed %s"><i class="fa fa-book"></i> <span>Inventario</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
      <div id="inventario" class="collapse ">
        <ul class="nav">
          <li><a href="nuevoproducto.php">Añadir producto</a></li>
          <li><a href="inventario.php">Inventario</a></li>
        </ul>
      </div>
    </li>
    <li>
      <a href="#ventas" data-toggle="collapse" class="collapsed %s"><i class="fa fa-eur"></i> <span>Ventas</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
      <div id="ventas" class="collapse ">
        <ul class="nav">
          <li><a href="nuevaventa.php">Nueva venta</a></li>
          <li><a href="historial.php" class="">Historial</a></li>
          <li><a href="balance.php" class="">Balance</a></li>
        </ul>
      </div>
    </li>
    <li>
      <a href="prestamos.php" class="%s"><i class="fa fa-gift"></i> <span>Prestamos</span></a></li>
    </li>
    <li>
      <a href="#seguridad" data-toggle="collapse" class="collapsed %s"><i class="fa fa-cog"></i> <span>wControl</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
      <div id="seguridad" class="collapse ">
        <ul class="nav">
          <li><a href="config-sitio.php" class="">Configuracion</a></li>
          <li><a href="cambiarpass.php">Cambiar contraseña</a></li>
          <li><a href="logs.php" class="">Log de movimientos</a></li>
          <li><a href="changelog.php" class="">Changelog</a></li>
        </ul>
      </div>
    </li>', $p1, $p2, $p3, $p4, $p5, $p6);
  } ?>


</ul>
