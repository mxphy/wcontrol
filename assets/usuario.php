<?php if(!isset($_SESSION['usuario']))
{
  header('Location: login.php');
  exit();
}
if($_SESSION['REMOTE_ADDR'] != $_SERVER['REMOTE_ADDR'] || $_SESSION['HTTP_USER_AGENT'] != $_SERVER['HTTP_USER_AGENT']) {
    exit();
} ?>
