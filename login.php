<?php
include_once "app/iniciar.php";
if(isset($_SESSION['usuario']))
{
  header('Location: index.php');
  exit();
}
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
WControl::LoginSeguro($_POST['user'], hash('sha512', $_POST["pass"]), $lin);
}
?>

<!DOCTYPE html>
<html lang="en" class="fullscreen-bg">
<head>
	<title>Iniciar | wControl</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- CSS -->
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/vendor/icon-sets.css">
	<link rel="stylesheet" href="assets/css/main.min.css">
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
</head>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<div class="vertical-align-wrap">
			<div class="vertical-align-middle">
				<div class="auth-box ">
					<div class="left">
						<div class="content">
							<div class="logo text-center"><img src="assets/img/logo-dark.png" alt="Klorofil Logo"></div>
              <?php if (isset($msg)) {WControl::MostrarError($msg);} ?>
							<form class="form-auth-small" action="<?= $_SERVER['PHP_SELF'] ?>" method="post">
								<div class="form-group">
									<label for="signup-email" class="control-label sr-only">Usuario</label>
									<input type="text" class="form-control" name="user" placeholder="Usuario">
								</div>
								<div class="form-group">
									<label for="signup-password" class="control-label sr-only">Contraseña</label>
									<input type="password" class="form-control" name="pass" placeholder="Contraseña">
								</div>

								<input type="submit" class="btn btn-primary btn-lg btn-block" value="Entrar">
								<div class="bottom">
									<span><i class="fa fa-question-circle"></i><a href="#"> ¿No recuerdas tu contraseña?</a></span><br>
                  <small>Versión v1.4 - Developed by BlackLeaf</small>
								</div>
							</form>
						</div>
					</div>
					<div class="right">
						<div class="content text">
						<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
							<center><small></small>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
	<!-- END WRAPPER -->
</body>

</html>
