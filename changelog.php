<?php
$webid = 'seguridad';
include_once "app/iniciar.php";

$fecha = WControl::ObtenerFecha();
?>

<!doctype html>
<html lang="en">

<head>
	<title>Changelog | wControl</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- CSS -->
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/vendor/icon-sets.css">
	<link rel="stylesheet" href="assets/css/<?php echo $color?>">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="assets/css/demo.css">
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
</head>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- SIDEBAR -->
		<div class="sidebar">
			<div class="brand">
				<a href="index.php">
				  <center>
				    <font color="white">
				  <h4><img src="assets/img/logoini.png"<br> <?php echo $nombreclub?></h4>
				</font>
				</center>
				</a>
			</div>
			<div class="sidebar-scroll">
				<nav>
					<?php include "assets/menu.php"; ?>
				</nav>
			</div>

		</div>
		<!-- END SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
			<!-- NAVBAR -->
			<nav class="navbar navbar-default">
				<div class="container-fluid">
					<div class="navbar-btn">
						<button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-arrow-left-circle"></i></button>
					</div>
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-menu">
							<span class="sr-only">Toggle Navigation</span>
							<i class="fa fa-bars icon-nav"></i>
						</button>
					</div>
					<div id="navbar-menu" class="navbar-collapse collapse">

						<ul class="nav navbar-nav navbar-right">


							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="assets/img/user.png" class="img-circle" alt="Avatar"> <span><?php echo $nombreclub?></span> <i class="icon-submenu lnr lnr-chevron-down"></i></a>
								<ul class="dropdown-menu">
									<li><a href="assets/logout.php"><i class="lnr lnr-exit"></i> <span>Salir</span></a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</nav>
			<!-- END NAVBAR -->
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
						<div class="panel panel-default">
						  <div class="panel-heading" style="text-align: center;">Changelog</div>
						  <div class="panel-body">
								<b>v1.4</b>
					          <ul>
					           <li>Agregado cambio de contraseña, ahora podrás cambiar la contraseña para acceder al wControl facilmente.</li>
					           <li>Ahora los datos de acceso están encriptados en SHA-512.</li>
					           <li>Se han tapado pequeñas vulnerabilidades y re-estructurado código para mayor seguridad.</li>
					           <li>Log de movimientos: ahora todos los movimientos que hagas desde wControl quedarán registrados en un LOG donde podrás revisarlos y ver
					            información estilo la hora del movimiento, quien lo hizo, que afectó, etc.</li>
					           <li>Configuración de wControl: ahora podrás decorar wControl con el nombre de tu social club, podrás añadir el logo de tu social club, cambiar
					            los colores del menú, etc.</li>
										 <li>Se ha adaptado el sistema de inventario para poder añadir otros productos por unidad.</li>
										 <li>Corrección de errores y optimización de código.</li>
					          </ul>
					          <hr>
										<b>v1.3</b>
										<ul>
											<li>Opción para poder agregar gastos del personal o por cualquier otra causa, así se puede tener un control total de el dinero actual.</li>
											<li>Ahora, si registras a un socio y es menor de edad, te lo detecta y te rechaza el registro.</li>
											<li>Opción para registrar los préstamos de objectos del club a los socios.</li>
											<li>Añadido el historial de compras de cada miembro en la búsqueda de socio por Nº socio.</li>
											<li>Añadido un historial de ventas del día en la sección de Inicio.</li>
											<li>Ahora al realizar una venta también tendrás que escribir el nombre de la persona que la realice.</li>
											<li>Corrección de errores y optimización de código.</li>
										</ul>
									  <hr>
										<b>v1.2</b>
										<ul>
											<li>Sistema de inventario añadido, ahora puedes añadir tu stock actual, asignarle un precio y añadir notas.</li>
											<li>Ahora puedes controlar, elimitar o modificar tu stock actual del sistema de inventario.</li>
											<li>Sistema de ventas añadido, ahora puedes realizar una venta asignando un número de socio. El precio de la venta se calculará automáticamente
												por el precio asignado en el stock del sistema de inventario.</li>
											<li>Ahora, cuando hagas una venta, la cantidad se descontará automáticamente del stock actual proporcionando la cantidad real de gramos que quedan.</li>
											<li>Ahora puedes añadir mas productos en una venta (máximo 10).</li>
											<li>Historial de ventas creado, ahora tendrás registradas todas las ventas y estadísticas de ventas totales y ventas diarias.</li>
											<li>Agregada barra rápida de búsqueda de socio (por número de socio).</li>
											<li>Corrección de errores y optimización de código.</li>
										</ul>
										<hr>
										<b>v1.1</b>
										<ul>
											<li>Sistema de socios añadido, ahora puedes registrar socios en la base de datos.</li>
											<li>Búsqueda de socios por número de socio añadida.</li>
											<li>Búsqueda de socios por nombre y apellidos añadida. Al poner solo un nombre, te saldrán todos los socios registrados con ese nombre.</li>
											<li>Edición de socios por número de socio, ahora podrás editar los datos de un socio registrado.</li>
											<li>Edición de socios por nombre y apellidos.</li>
											<li>Ahora puedes borrar un socio registrado.</li>
											<li>Corrección de errores y optimización de código.</li>
										</ul>
										<hr>
										<b>v1.0</b>
										<ul>
											<li>Adaptación y edición del diseño (créditos por el template: <a href="https://themeineed.com">ThemeINeed</a>)</li>
											<li>Creación de la base de datos</li>
											<li>Sistema de logín</li>
										</ul>
						</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
			<footer>
				<div class="container-fluid">

				</div>
			</footer>
		</div>
		<!-- END MAIN -->
	</div>
	<!-- END WRAPPER -->
	<!-- Javascript -->
	<script src="assets/js/jquery/jquery-2.1.0.min.js"></script>
	<script src="assets/js/bootstrap/bootstrap.min.js"></script>
	<script src="assets/js/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="assets/js/klorofil.min.js"></script>
</body>

</html>
