<?php
$webid = 'inventario';
include_once "app/iniciar.php";
if (isset($_GET['edit'])) {
	if ($_GET['edit'] == "true") {
		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		  $sql = "UPDATE `inventario` SET `nombre` = '".$_POST['nombrep']."',
		  `precio` = '".$_POST['preciop']."',
		  `gramos` = '".$_POST['stockp']."',
		  `nota` = '".$_POST['notap']."' WHERE `inventario`.`id` = ".$_POST['editid']."";
		  $query = mysql_query($sql);
		  if ($query === false) {
		      echo "Could not successfully run query ($sql) from DB: " . mysql_error();
		      exit;
		  }
		  else {
		    $msg = "¡Exito! El producto fue editado correctamente.";
				WControl::EscribirLog("Se ha editado el producto ".$_POST['nombrep'].", ahora tiene ".$_POST['stockp']." unidades/gramos y al precio de ".$_POST['preciop']."€ por unidad/gramo.");
		  }
		}
	}
}
if ($_SERVER['REQUEST_METHOD'] == 'POST' && !empty($_POST['cantidad'])) {
$stocknuevo = $_POST['cantidad'];
$sql = "SELECT * FROM inventario WHERE id = '".$_POST['producto']."'";
$query = mysql_query($sql);
while ($row = mysql_fetch_assoc($query)) {
		$stockanterior = $row['gramos'];
		$nombreprod = $row['nombre'];
}
$stockfinal = $stockanterior + $stocknuevo;
$sql = "UPDATE `inventario` SET `gramos` = '$stockfinal' WHERE `inventario`.`id` = '".$_POST['producto']."';";
$query = mysql_query($sql);
if ($query === false) {
		$error = "Ocurrio un error al actualizar el stock del producto.";
}
else {
	$exito = "¡Exito! Se ha añadido $stocknuevo unidades/gramos al producto, ahora tiene $stockfinal g.";
	WControl::EscribirLog("Se han añadido ".$stocknuevo." unidades/gramos a el producto ".$nombreprod.", ahora ".$nombreprod." tiene ".$stockfinal." unidades/gramos.");
}
}

?>

<!doctype html>
<html lang="en">

<head>
	<title>Inventario | wControl</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- CSS -->
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/vendor/icon-sets.css">
	<link rel="stylesheet" href="assets/css/<?php echo $color?>">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="assets/css/demo.css">
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
</head>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- SIDEBAR -->
		<div class="sidebar">
			<div class="brand">
				<a href="index.php">
				  <center>
				    <font color="white">
				  <h4><img src="assets/img/logoini.png"<br> <?php echo $nombreclub?></h4>
				</font>
				</center>
				</a>
			</div>
			<div class="sidebar-scroll">
				<nav>
					<?php include "assets/menu.php"; ?>
				</nav>
			</div>

		</div>
		<!-- END SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
			<!-- NAVBAR -->
			<nav class="navbar navbar-default">
				<div class="container-fluid">
					<div class="navbar-btn">
						<button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-arrow-left-circle"></i></button>
					</div>
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-menu">
							<span class="sr-only">Toggle Navigation</span>
							<i class="fa fa-bars icon-nav"></i>
						</button>
					</div>
					<div id="navbar-menu" class="navbar-collapse collapse">

						<ul class="nav navbar-nav navbar-right">


							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="assets/img/user.png" class="img-circle" alt="Avatar"> <span><?php echo $nombreclub?></span> <i class="icon-submenu lnr lnr-chevron-down"></i></a>
								<ul class="dropdown-menu">
									<li><a href="assets/logout.php"><i class="lnr lnr-exit"></i> <span>Salir</span></a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</nav>
			<!-- END NAVBAR -->
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<center>
						<?php if (isset($_GET['error'])) {
							if ($_GET['error'] = "Ineditable") {
								echo '<div class="alert alert-danger" role="alert">Error: Este producto no se puede modificar (No existe).</div>';
							}
						}
						if (isset($msg)) {
							printf('<div class="alert alert-info" role="alert">%s</div>', $msg);
						}?>
					<h3 class="page-title">Inventario</h3>
					<div class="panel panel-headline">
						<div class="panel-body">
              <div class="panel panel-default">

  <!-- Default panel contents -->
  <div class="panel-heading">Lista de articulos</div>

  <!-- Table -->
  <table class="table">
    <thead>
      <tr>
        <th>#</th>
        <th>Nombre</th>
        <th>Precio por unidad/gramo</th>
        <th>Stock en unidades/gramos</th>
        <th>Acciones</th>
      </tr>
    </thead>
    <tbody>
      <?php $sql = "SELECT * FROM inventario";
        $query = mysql_query($sql);
        if ($query === false) {
            echo "<option>Could not successfully run query ($sql) from DB: " . mysql_error();
            echo "</option>";
            exit;
        }
        while ($row = mysql_fetch_assoc($query)) {
          $pnum = $row['id'];
          $pnombre = $row['nombre'];
          $pprecio = $row['precio'];
          $pgramos = $row['gramos'];
          printf('<tr>
            <th scope="row">%s</th>
            <td>%s</td>
            <td>€ %s</td>
            <td>%s</td>
            <td><a href="editarproducto.php?editid=%s" class="btn btn-info"><i class="fa fa-edit"></i></a><a href="borrarproducto.php?prod=%s" class="btn btn-danger"><i class="fa fa-trash"></i></a></td>
          </tr>', $pnum, $pnombre, $pprecio, $pgramos, $pnum, $pnum);
        } ?>


    </tbody>
  </table>
</div>


						</div>
					</div>
					<?php if (isset($exito)) {
						printf('<div class="alert alert-info" role="alert">%s</div>', $exito);
					}?>
					<?php if (isset($error)) {
						printf('<div class="alert alert-danger" role="alert">Error: %s</div>', $error);
					} ?>

          <h3 class="page-title">Añadir Stock</h3>
					<div class="panel panel-headline">
						<div class="panel-body">
              <div class="panel panel-default">
                <div class="form-group">
                  <div class="row">
										<form class="" action="<?= $_SERVER['PHP_SELF']?>" method="post">
                    <div class="col-md-8">
                        <label class="control-label">Producto</label>
                        <select class="form-control" style="text-align: center;" name="producto">
                          <?php $sql = "SELECT * FROM inventario";
                            $query = mysql_query($sql);
                            if ($query === false) {
                                echo "<option>Could not successfully run query ($sql) from DB: " . mysql_error();
                                echo "</option>";
                                exit;
                            }
                            while ($row = mysql_fetch_assoc($query)) {
                              $numproducto = $row['id'];
                              $nombreproducto = $row['nombre'];
                              printf('<option value="%s">%s - %s</option>', $numproducto, $numproducto, $nombreproducto);
                            }?>

                        </select> </div>
                    <div class="col-md-3">
                        <label class="control-label">Cantidad en unidades/gramos</label>
                        <input type="number" placeholder="0.3" class="form-control" name="cantidad" step="any" /> </div>
                    <div class="col-md-1">
                      <label class="control-label">-</label>
                        <button type="submit" class="btn btn-danger">
                            <i class="fa fa-download"></i>
                        </button>
                    </div>
										</form>
                  </div>
                </div>
              </div>
            </div>
          </div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
			<footer>
				<div class="container-fluid">
					<p class="copyright">&copy; wControl 2017 - Developed by BlackLeaf (<?php echo $version?>)</p>
				</div>
			</footer>
		</div>
		<!-- END MAIN -->
	</div>
	<!-- END WRAPPER -->
	<!-- Javascript -->
	<script src="assets/js/jquery/jquery-2.1.0.min.js"></script>
	<script src="assets/js/bootstrap/bootstrap.min.js"></script>
	<script src="assets/js/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="assets/js/klorofil.min.js"></script>
</body>

</html>
